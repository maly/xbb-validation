#!/usr/bin/env python3

"""
Makes Histograms for Discriminants to use in making ROC curves and store them in H5 file
The y-axis of these histograms are jets satisfying pre-selection and in case of higgs and
top, also satisfying a truth-matching requirement (by ghost-association)

Returns an H5 file which is split into the following structure:
    - 1 Group per pT range studied:
        - 1 Group per discriminant used:
            - 1 Dataset of discrim bin-values per process (x3)
            - 1 Dataset with discrim x-axis edges
"""

from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import json
from pathlib import Path

from xbb.var_getters import DISCRIMINANT_GETTERS, DISCRIMINANT_EDGES

from xbb.common import get_ds_weights_dict, get_dsid, get_pt_eta_reweighting
from xbb.common import dhelp

from xbb.selectors import is_dijet, is_ditop, is_dihiggs,  PROC_SELECTORS
from xbb.selectors import (mass_window_higgs, fatjet_mass_window_pt_range,
                           fatjet_mass_window_pt_range_truth_match)

# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_VERBOSE_HELP = 'Turn on Verbose run'
_WEIGHTS_HELP = 'The .json file containing DSID:Sample Weight Map '
_PT_ETA_HISTS_HELP = dhelp('Path to where the histograms fromjet_pt_eta_reweighting.py are stored')
_OUTFILE_HELP = dhelp('Path to (including) file where ROC curves are stored ')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
_ROC_DISC_HELP = dhelp('The ROC discriminants to plot curves for')
# ================
# Defaults for Args
# =================
DEFAULT_PT_RANGE = (250e3, np.inf)
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025','Xbb_V3_ftop025', 'dl1r'}
DISC_CHOICES = set(DISCRIMINANT_GETTERS.keys())
# ================


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-w', '--weights', required=True, help=_WEIGHTS_HELP)
    parser.add_argument('-i', '--path-to-pt-eta-hists', default='pt-eta-hists',
                        type=Path, help=_PT_ETA_HISTS_HELP)
    parser.add_argument('-o', '--out-file', default='roc1d.h5',
                        type=Path, help=_OUTFILE_HELP)
    parser.add_argument('-p', '--pt-range', nargs=2, type=float,
                        default=DEFAULT_PT_RANGE, help=_PT_RANGE_HELP)
    parser.add_argument('-r', '--roc-discriminants', nargs='+', choices=DISC_CHOICES,
                        default=DEFAULT_DISCRIMS, metavar='DISCRIM', help=_ROC_DISC_HELP)
    parser.add_argument('-v', '--verbose', action='store_true', help=_VERBOSE_HELP)
    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():

    # Get args
    args = get_args()
    # ================
    # Prepare Output directory
    # ================#
    # Get out-dir
    outfile = args.out_file
    outfile.parent.mkdir(parents=True, exist_ok=True)
    if(outfile.is_file()):
        outfile.unlink()
    # ================
    # Prepare fat-jet selectors
    # Must later pass the fat-jets datsets to them
    # ================
    pt_range = args.pt_range
    dijet_selector = fatjet_mass_window_pt_range(pt_range, mass_window_higgs)
    higgs_selector = fatjet_mass_window_pt_range_truth_match(pt_range, mass_window_higgs,
                                                             {'GhostHBosonsCount': 1})
    top_selector = fatjet_mass_window_pt_range_truth_match(pt_range, mass_window_higgs,
                                                           {'GhostTQuarksFinalCount': 1})
    # ================
    # Process and save output per process
    # ================
    run_process('dijet', args, dijet_selector)
    run_process('higgs', args, higgs_selector)
    run_process('top', args, top_selector)


# ==================================================================
# Function to run a process, which means getting the histogram with appropriate settings
# and initiate save output
# ==================================================================
def run_process(process, args, selection):
    # ================
    # Get all args
    # ================
    # Be verbose?
    verbose = args.verbose
    # Get the destination directory
    outfile = args.out_file
    # The pt_eta_histos file to output to then use for reweights
    pt_eta_hists_file = args.path_to_pt_eta_hists/'jet_pt_eta.h5'
    # Overall sample weights file
    sample_weights_file = args.weights
    # Get the sample weights from file
    with open(sample_weights_file, 'r') as ds_wts_file:
        sample_weights = get_ds_weights_dict(ds_wts_file)
    # The sample datasets to process
    datasets = args.datasets
    # get only datasets relevant to the current process
    valid_datasets = [ds for ds in datasets
                      if PROC_SELECTORS[process](get_dsid(ds))
                      and np.isfinite(sample_weights[get_dsid(ds)])]
    # Discriminants to Study
    discrims_of_interest = args.roc_discriminants
    # pT range
    pt_range = args.pt_range
    # ================
    # Process the samples
    # ================
    if verbose:
        print(f'Processing {process} samples')
    # Loop over all discriminants
    for discrim_name in discrims_of_interest:
        if verbose:
            print(f'running {discrim_name}')
        # ================
        # Initialize Stuff
        # ================
        # Initialize the histogram
        discrim_hist = 0
        discrim_hist_map = {}
        # Get pre-defined edges for discrim Histograms, and add overflow and underflow bins
        edges = np.concatenate([np.array([-np.inf]),
                                DISCRIMINANT_EDGES[discrim_name],
                                np.array([np.inf])])
        # Get the discriminant
        discrim_getter = DISCRIMINANT_GETTERS[discrim_name]
        # Loop over datasets for this process
        for ds in valid_datasets:
            if args.verbose:
                print(f'running on {ds} as {process}')

            # Get the histogram for this dataset (a numpy array with bin-values)
            this_dsid_discrim_hist = get_hist(ds, edges, pt_eta_hists_file,
                                              discrim_getter, selection,
                                              process, sample_weights)

            # sum all histograms for non-dijet into a total discrim histogram
            discrim_hist += this_dsid_discrim_hist
        # Make a discrim name to histo map
        discrim_hist_map[discrim_name] = discrim_hist
        # Save the histogram to file
        save_hist(process, discrim_hist_map, edges, outfile, pt_range)


# ==================================================================
# Function which gets discriminant histogram bin-value for each sample dataset
# ==================================================================
def get_hist(ds, edges, pt_eta_hists_file, discrim_getter, selection, process, sample_weights):

    # ==================================================================
    # Getting re-weights for the histogram
    # ==================================================================
    (pt_reweighting, eta_reweighting,
        pt_edges, eta_edges) = get_pt_eta_reweighting(pt_eta_hists_file, process, get_edges=True)
    # Initialize the histograms
    discrim_hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath, 'r') as h5file:
            # Get fat-jets
            fat_jets = np.asarray(h5file['fat_jet'])
            # Get fat-jets pT array
            pt = fat_jets['pt']
            # Get the bin of each pT entry (length = pt array length)
            # if pt = (pt1, pt2) & pt1 in bin 1, pt2 in bin 5, then indicies = (1,5)
            indices_pt = np.digitize(pt, pt_edges) - 1
            # Overall event weight, for dijet = L*XS*filt_Eff / N_{MC}, 1 otherwise
            sample_weight = sample_weights[get_dsid(ds)]
            # Re-define weight as the qcd/proc ratio and re-size it to match pt array size
            # if ratio = [r1, r2, r3, r4, r5] and indices=[3,3,2,4,1,2,2,1,1,4,4] (size = pt.size())
            # weight = [r3, r3, r2, r4, r1, r2, r2, r1, r1, r4, r4] (size =pt.size())
            weight = pt_reweighting[indices_pt]*sample_weight*fat_jets['mcEventWeight']
            # Get the array of discriminant values for each fat-jet
            discrim = discrim_getter(h5file)
            # Apply the selector function to the fat-jet
            sel = selection(fat_jets)
            # Make the numpy histogram and get the bin-values from it (element [0])
            # filtering disc array uses numpy bool indexing
            discrim_hist += np.histogram(discrim[sel], edges, weights=weight[sel])[0]

    return discrim_hist


# ==================================================================
# Function which writes out contents pf dict (discrim_name, (process_name, discrim_histo)
# to an h5 file, with groups_names === discrim_name and inside them
# datasets_names == process_names whose data == discrim_histo
# ==================================================================
def save_hist(process, discrim_hist_map, edges, outfile, pt_range):
    import re
    args = dict(dtype=float)
    with File(outfile, 'a') as out_file:

        # Make a group named pT range to hold 1 group per discrim to hold 1 dataset per process
        pt_str_gev = '_'.join('{:.0f}'.format(x*1e-3) for x in pt_range)
        out_group = out_file.require_group(f'/{pt_str_gev}')
        out_group.attrs['pt_range'] = pt_range
        discrim_name = [*discrim_hist_map][0]
        grp = out_group.require_group(discrim_name)
        grp.create_dataset(process, data=discrim_hist_map[discrim_name], **args)
        # Only one edge per discrim
        try:
            grp.attrs['edges']
        except:
            grp.attrs['edges'] = edges


if __name__ == '__main__':
    run()
