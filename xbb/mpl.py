"""
This is responisble for building methods that can be used
in styling a matplotlib figure with some ATLAS settings

It also contains a definition of a Canvas class and
CanvasWithRatio class that are ATLAS suitable
"""

import os

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import FuncFormatter
from matplotlib.figure import Figure
from matplotlib import gridspec
from matplotlib.artist import setp
from matplotlib.ticker import AutoMinorLocator
import matplotlib.ticker

import math
import numpy as np

# ==============================================
# ================== Classes ===================
# ==============================================
# Canvas class for a normal plot without ratio panel
# ==============================================
class Canvas:
    # Set some default filename in case none provided
    default_name = 'Canvas.pdf'

    # Class constructor
    def __init__(self, out_path=None, figsize=(15, 10), fontsize=18):
        # Build a figure with requested size
        self.fig = Figure(figsize)
        # Choose a tight-layout to reduce white space
        self.fig.set_tight_layout(True)
        # Get the Canvas containing the figure
        self.canvas = FigureCanvas(self.fig)
        # Get the axes to plot on
        self.ax = self.fig.add_subplot(1, 1, 1)
        # Get the output file path
        self.out_path = str(out_path)

        # Style the axies with ATLAS settings
        atlas_style(self.ax, fontsize=fontsize)

    # Define a saving method to write out the plot
    def save(self, out_path=None):
        # Make sure there is a file name provided either
        # via a direct call to this func or via construct
        output = out_path if out_path is not None else self.out_path
        assert output, "An output file name is required"

        # Sepearate file name and directory
        out_dir, out_file = os.path.split(output)
        # Make the output directory if not there
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        # Now we print out the Canvas onto the file
        self.canvas.print_figure(output, bbox_inches='tight')

    # ================
    # Methods to use canvas in "with" statements
    # ================#
    # Method called at start of "with" block
    def __enter__(self):
        # Set the outpath using constructor or default name
        if not self.out_path:
            self.out_path = self.default_name
        # return the Canvas
        return self

    # Method called at end of "with" block
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        # Save the Canvas in the outpath
        self.save(out_path=self.out_path)
        return True


# ==============================================
# Canvas class for a plot with ratio panel
# ==============================================
class CanvasWithRatio:
    # Set some default filename in case none provided
    default_name = 'CanvasWithRatio.pdf'

    # Class constructor
    def __init__(self, out_path=None, figsize=(15, 10)):
        # Build a figure with requested size
        self.fig = Figure(figsize)
        # Get the Canvas containing the figure
        self.canvas = FigureCanvas(self.fig)
        # GridSpec customizes location of subplots
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        # Make axes on the subplots
        self.ax = self.fig.add_subplot(gs[0])
        # Ratio panel shares x-axis of "ax"
        self.ax2 = self.fig.add_subplot(gs[1], sharex=self.ax)
        # Adjust the height of space between subplots
        self.fig.subplots_adjust(hspace=0.1)
        # Get the output file path
        self.out_path = str(out_path)
        # Style the axies with ATLAS settings
        atlas_style(self.ax)
        atlas_style(self.ax2)

        # hide the x-tick lables on the main plot
        setp(self.ax.get_xticklabels(), visible=False)

    # Define a saving method to write out the plot
    def save(self, out_path=None):
        # Make sure there is a file name provided either
        # via a direct call to this func or via construct.
        output = out_path if out_path is not None else self.out_path
        assert output, "an output file name is required"
        # Sepearate file name and directory
        out_dir, out_file = os.path.split(output)
        # Make the output directory if not there
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        # Now we print out the Canvas onto the file
        self.canvas.print_figure(output, bbox_inches='tight')
    # ================
    # Methods to use canvas in "with" statements
    # ================
    # Method called at start of "with" block

    def __enter__(self):
        # Set the outpath using constructor or default name
        if not self.out_path:
            self.out_path = self.default_name
        # return the Canvas
        return self

    # Method called at end of "with" block
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        # Save the Canvas in the outpath
        self.save(out_path=self.out_path)
        return True


# ==================================================================
# ATLAS Axes styling. This is standard and shouldn't need to changE
# ==================================================================
def atlas_style(ax, fontsize=18):
    where = {x: True for x in ['bottom', 'top', 'left', 'right']}
    ax.tick_params(which='both', direction='in', labelsize=fontsize, **where)
    ax.tick_params(which='minor', length=6)
    ax.tick_params(which='major', length=12)
    ax.grid(True, which='major', alpha=0.10)
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))


# ==============================================
# ============= AddInfo to Canvas =============
# ==============================================
# ATLAS Label
# ==============================================
def add_atlas_label(ax, x=0.05, y=0.91, sort_of_internal=False,
                    internal=True, vshift=0.08,
                    add_kinematic_info=False):

    text_fd = dict(ha='left', va='center')
    atlas_fd = dict(weight='bold', style='italic', size=32, **text_fd)
    internal_fd = dict(size=30, **text_fd)
    info_df = dict(size=22, **text_fd)
    label_break_ratio_hack = 1.0
    shift = 0.15 * label_break_ratio_hack
    ax.text(x, y, 'ATLAS', fontdict=atlas_fd, transform=ax.transAxes)
    if(sort_of_internal):
        ax.text(x+shift, y,
                'sort of Internal',
                fontdict=internal_fd,
                transform=ax.transAxes)
    else:
        ax.text(x+shift, y,
                'Simulation Internal' if internal else 'Simulation Preliminary',
                fontdict=internal_fd,
                transform=ax.transAxes)

    ax.text(x, y-vshift,
            r'$\sqrt{s}=13$ TeV',
            fontdict=info_df,
            transform=ax.transAxes)


# The ATLAS label inside a box
def add_atlas_label_in_box(ax, x, y, internal=True, size=28):
    post = 'Simulation Internal' if internal else 'Simulation Preliminary'
    text_fd = dict(ha='left', va='top', size=size)
    bbox = dict(boxstyle='round', fc='w')
    atlas_fd = dict(weight='bold', style='italic', bbox=bbox, **text_fd)
    text = 'ATLAS' + ' '*int(1.7*(len(post)+1))
    ax.text(x, y, text, transform=ax.transAxes, **atlas_fd)
    ax.text(x+0.17, y, post, transform=ax.transAxes, **text_fd)


# ==================================================================
# Add the kinematic cuts info (preselection + pt_range + mass + efficiencies )
# ==================================================================
def add_kinematic_acceptance(ax, x=0.05, y=0.05, pt_range=None, eff=None,
                             offset=0.05, mass_in_presel=True, in_a_box=False):
    text_settings = dict(transform=ax.transAxes, size=24, ha='left', va='center')
    # Add masss window
    if(mass_in_presel):
        mass = r'$76 < m_{J}\,/\,{\rm GeV} < 146$'
    else:
        mass = ''  #r'$50 < m_{J}\,/\,{\rm GeV} < 300$'
    if(not in_a_box):
        ax.text(x, y, mass, **text_settings)
        y += offset
    # Add eta cut
    eta = r'$|\eta_{\rm J}| < 2.0$'
    if(not in_a_box):
        ax.text(x, y, eta, **text_settings)
        y += offset
    # If a pT range is provided print it
    ptj = r'p_{\rm T}^{\rm J}'
    gev = r'\,{\rm GeV}'
    if pt_range is not None:
        # If range provided as a tuple
        if(isinstance(pt_range, str)):
            pt_min, pt_max = pt_range.split("_")
            if(pt_max == "inf"):
                pt_string = rf'${ptj} > {pt_min}{gev}$'
            else:
                pt_string = rf'${pt_min}{gev} < {ptj}\,/\,{gev} < {pt_max}$'
        # if range provided as a string "ptmin_ptmax"
        else:
            pt_min, pt_max = ('{:.0f}'.format(x*1e-3) for x in pt_range)
            if(math.isinf(float(pt_max))):
                pt_string = rf'${ptj} > {pt_min}{gev}$'
            else:
                pt_string = rf'${pt_min}{gev} < {ptj}\,/\,{gev} < {pt_max}$'
    # Otherwise use the default 250 GeV cut
    else:
        pt_string = rf'${ptj} > 250{gev}$'
    if(not in_a_box):
        ax.text(x, y, pt_string, **text_settings)
        y += offset
    # If an efficiency cut is applied, use it!
    if eff is not None:
        eff_num = '{:.1f}'.format(eff)
        eff_string = r'$\epsilon_{\rm Higgs}$ = ' + eff_num
        if(not in_a_box):
            ax.text(x, y, eff_string, **text_settings)

    if(in_a_box):
        box_props = dict(boxstyle='round', facecolor='w')
        # Text settings

        text_settings = dict(transform=ax.transAxes, size=18,
                             ha='left', va='bottom', bbox=box_props)
        text = (
            r'$\sqrt{s} = 13\,{\rm TeV}$' + '\n'
            + 'Selection:\n'
            + eta + '\n'
            + mass + '\n'
            + pt_string + '\n')[:-1]
        ax.text(x, y, text, **text_settings)


# ==============================================
# ============= Axes settings =============
# ==============================================
def helvetify():
    """
    Load 'Helvetica' default font (may be Arial for now)
    """
    from matplotlib import rc
    # 'Comic Sans MS', 'Trebuchet MS' works, Arial works in png...
    rc('font', **{'family': 'sans-serif', 'sans-serif': ['Arial']})


def log_formatting(value, pos):
    """
    This is for use with the FuncFormatter.

    It writes 1 and 10 as plane numbers, everything else as exponential.
    """
    roundval = round(value)
    if roundval == 1:
        base = 1
        exp = ''
    elif roundval == 10:
        base = 10
        exp = ''
    else:
        base = 10
        exp = round(math.log(value, 10))
    return r'{}$^{{\mathdefault{{ {} }} }}$'.format(base, exp)


def xlabdic(fontsize=20):
    # options for the x axis
    return dict(ha='right', x=0.98, fontsize=fontsize)


def ylabdic(fontsize=20):
    # options for the y axis
    return dict(ha='right', y=0.98, fontsize=fontsize)


def log_style(ax):
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(FuncFormatter(log_formatting))
    y_minor = matplotlib.ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
    ax.yaxis.set_minor_locator(y_minor)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    ax.grid(True, which='minor', axis='y', alpha=0.03)