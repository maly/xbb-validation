#!/usr/bin/env python3

"""Rename files to give them more useful names.

This won't actually touch the original files, it just creates another
directory with simlinks to them. Requires that you have a file that
lists the input DAODs, and a file that lists the outputs from those
jobs.

Using ds == dataset
"""
from argparse import ArgumentParser
from pathlib import Path
from os.path import relpath
import os.path
from datetime import datetime
# ==================================================================
# Get arguments
# ==================================================================
_help_input_list = "list of input DxAODs"
_help_h5_names = "list of datasets derived from DxAODs"
_help_dir = "directory where downloaded datasets live"
_help_link_dir = "directory where links will go (with short names)"


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_file_names', help=_help_input_list)
    parser.add_argument('h5_dataset_names', nargs='?', help=_help_h5_names)
    parser.add_argument('h5_file_dir', nargs='?', type=Path, help=_help_dir)
    parser.add_argument('-o', '--short-files-dest-dir', type=Path, help=_help_link_dir)
    return parser.parse_args()


# ==================================================================
# A map between r-tag and campaign label
# ==================================================================
CAMPAIGN_MAP = {
    'r9315': 'mc16a',
    'r9364': 'mc16a',
    'r10210': 'mc16d',
    'r10201': 'mc16d',
    'grp15_v02': 'data15'
}


def get_campaign(tags):
    campaign = set(CAMPAIGN_MAP[x] for x in CAMPAIGN_MAP if x in tags)
    if len(campaign) != 1:
        raise LookupError(f'bad bad bad, unknown campaign: {tags}')
    return campaign.pop()


# ==================================================================
def run():
    args = get_args()
    short_names = []  # list to hold the new directories names
    # Open the list of names of DAOD samples
    with open(args.input_file_names) as input_files:
        for line in input_files:
            # Extraxct all info spearated by "." in name (prefix,disd,generator_process_name,"deriv", DAOD_name,AMI_Tag)
            pfx, dsid, longname, step, deriv, tags = line.strip().split('.')
            # split the generator name from process name and choose process name
            process_label = longname.split('_', 2)[-1]
            # get campaign
            campaign = get_campaign(tags)
            # short name is e.g. mc16a.my_process.12345
            short_names.append('.'.join([campaign, process_label, dsid]))
    # Handle if list of H5 files is not there
    if not args.h5_dataset_names:
        print("Cannot find a list of H5 files ... provide one for following samples: ")
        for short in short_names:
            print(short)
        print("Exiting....")
        exit(0)
    # Otherwise we loop over H5 files
    with open(args.h5_dataset_names) as h5_ds_names:
        # get dataset and remove spaces before and after lines
        h5_ds_names = [l.strip() for l in h5_ds_names]

    # =============================
    # checks on validity compatibility between H5 files and DAODs
    # =============================
    # Check if both lists are of same length
    if len(h5_ds_names) != len(short_names):
        raise LookupError('number of short names does not match longer names')

    # Check all DSIDs in H5 list are in DAOD list and vice versa
    #
    # Get the DISDs of each H5 dataset and DAOD provided
    h5_ds_dsids = [h5_ds_name.split('.')[2] for h5_ds_name in h5_ds_names]
    DAODs_dsids = [short_name.split('.')[-1] for short_name in short_names]
    # Check if there is any DSID in short names (i.e. DAODs) which is not in h5 list (& vice versa)
    any_DAOD_not_in_h5_list = any(dsisd not in h5_ds_dsids for dsisd in DAODs_dsids)
    any_h5ds_not_in_DAOD_list = any(dsid not in DAODs_dsids for dsid in h5_ds_dsids)
    # Raise error if samples in both lists are not identical
    if(any_DAOD_not_in_h5_list or any_h5ds_not_in_DAOD_list):
        raise LookupError(f' Make sure *input*.txt and *h5*.txt have same samples')
    # =============================
    # Create a name-map from H5 file name to short name
    # =============================
    name_map = {}
    h5_ds_name_stripped = [h5_ds_name.rstrip('/') for h5_ds_name in h5_ds_names]
    for h5_ds_name in h5_ds_names:
        for short_name in short_names:
            if h5_ds_name.split('.')[2] != short_name.split('.')[2]:
                continue
            else:
                # remove trailing "/" from H5 file names
                name_map[h5_ds_name.rstrip('/')] = short_name
    # =============================
    # Save Symlink to all H5 files inside directories outdir/short_name/
    # =============================
    # Check if path to downloaded H5 files is provided
    if not args.h5_file_dir:
        print("Cannot find directory where H5 files are ... Need the following files: ")
        for from_ds, to_ds in name_map.items():
            print(f'{from_ds} -> {to_ds}')
        print("Exiting....")
        exit(0)

    # If a destination directory is provided use it
    if args.short_files_dest_dir:
        dest_dir = args.short_files_dest_dir
    else:
        # Otherwise make a directory inplace named after the location of H5 files
        dest_dir = args.h5_file_dir.parent.joinpath('short_names')
    # Create the directory (along with parent directories needed if not there)
    if(os.path.isdir(dest_dir)):
        print("output directory exists ... creating a unique one using date&time : ")
        dest_dir = dest_dir.parent.joinpath('{}_{}'.format(args.short_files_dest_dir, datetime.now().strftime('%Y%m%d%H%M')))
        print("Created .... ", dest_dir)
    dest_dir.mkdir(parents=True)
    # Loop over all H5 datasets (.h5 files inside those) where they are downloaded
    for dataset_path in args.h5_file_dir.glob('user.*'):
        # Check the downloaed H5 dataset is in the list provided
        if(dataset_path.name not in h5_ds_name_stripped):
            continue
        # get the short name
        short_name = name_map[dataset_path.name]
        # make a directory with name = <short_name> inside dest_dir
        short_path = dest_dir.joinpath(short_name)
        short_path.mkdir(exist_ok=True)  # override if the directory is there
        # loop over .h5 files in each dataset and creare a symlink to it in dest_dir/<short_name>/
        for data_file in dataset_path.glob('*.h5'):
            link = short_path.joinpath(data_file.name)
            link.symlink_to(relpath(data_file, link.parent))


if __name__ == '__main__':
    run()
