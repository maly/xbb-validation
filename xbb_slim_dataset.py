#!/usr/bin/env python3

"""
Make slim datasets
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import json
import os
from pathlib import Path


def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('dataset', type=Path)
    parser.add_argument('-o', '--out-dir', default='slimmed', type=Path)
    parser.add_argument('-f', '--force', action='store_true')
    return parser.parse_args()


def define_variables(is_data=False):
    
    SUBJET_VARS = [f'DL1r_p{x}' for x in 'cub']
    if not is_data: SUBJET_VARS += ["GhostBHadronsFinalCount", "GhostCHadronsFinalCount", "HadronConeExclTruthLabelID", "HadronConeExclExtendedTruthLabelID"]
    
    SUBJET_VARS_OLD = ['MV2c10_discriminant']
    
    JSS_VARS = ["Split12",  "Split23",  "Qw",  "PlanarFlow",  "Angularity",
    "Aplanarity",  "ZCut12",  "KtDR", "C2",  "D2",  "e3",  "Tau21_wta",  "Tau32_wta",  "FoxWolfram20"]

    JET_LABELS = []
    if not is_data: JET_LABELS += [f'Ghost{p}BosonsCount' for p in 'HWZ'] + ['GhostTQuarksFinalCount']

    N_SUBJETS = 3
    
    DATASETS_N_VARS = {
        'fat_jet': [
            'pt', 'eta', 'mass', 'nVRSubjets', 'mcEventWeight', 'eventNumber',
            'averageInteractionsPerCrossing', 'actualInteractionsPerCrossing',
            *[f'Xbb202006_{x}' for x in ['Top', 'QCD', 'Higgs']],
            *[f'Xbb2020v2_{x}' for x in ['Top', 'QCD', 'Higgs']],
        ]+JSS_VARS+JET_LABELS,
        #**{f'subjet_VRGhostTag_{n+1}': SUBJET_VARS for n in range(N_SUBJETS)}, # unfortunately this is missing from data
        #**{f'subjet_FR_{n+1}': SUBJET_VARS_OLD for n in range(N_SUBJETS)},
        #**{f'subjet_VR_{n+1}': SUBJET_VARS_OLD for n in range(N_SUBJETS)}
    }

    if not is_data: 
        DATASETS_N_VARS = {**DATASETS_N_VARS, **{f'subjet_VRGhostTag_{n+1}': SUBJET_VARS for n in range(N_SUBJETS)}}
        DATASETS_N_VARS = {**DATASETS_N_VARS, **{f'subjet_VR_{n+1}': SUBJET_VARS_OLD for n in range(N_SUBJETS)}}
    else:
        DATASETS_N_VARS = {**DATASETS_N_VARS, **{f'subjet_VR_{n+1}': SUBJET_VARS for n in range(N_SUBJETS)}}

    
    return DATASETS_N_VARS

def build_slimmed_file_for(ds, out_ds, is_data=False):
    md_key = 'metadata'
    paths = list(ds.glob('*.h5'))
    out_datasets = {}
    DATASETS_N_VARS = define_variables(is_data)
    with File(paths[0], 'r') as base:
        for name, dsv in DATASETS_N_VARS.items():
            out_datasets[name] = OutputDataset(base[name], out_ds, name, dsv)
        mdb = base[md_key]
        metadata = OutputDataset(mdb, out_ds, md_key, mdb.dtype.names)
    for fpath in paths:
        with File(fpath, 'r') as infile:
            for ds_name, ods in out_datasets.items():
                ods.add(infile[ds_name])
            metadata.add(infile[md_key])

def build_skimmed_file_for(ds, out_ds, mask, is_data=False):
    md_key = 'metadata'
    paths = ds
    out_datasets = {}
    DATASETS_N_VARS = define_variables(is_data)
    with File(paths, 'r') as infile:
        for name, dsv in DATASETS_N_VARS.items():
            out_datasets[name] = OutputDataset(infile[name], out_ds, name, dsv)
        mdb = infile[md_key]
        metadata = OutputDataset(mdb, out_ds, md_key, mdb.dtype.names)
        for ds_name, ods in out_datasets.items():
            ods.add_masked(infile[ds_name],mask)
        metadata.add(infile[md_key])

class OutputDataset:
    def __init__(self, base_ds, out_group, output_name, variables):
        types = [(x, base_ds.dtype[x]) for x in variables]
        self.dataset = out_group.create_dataset(
            output_name, (0,), maxshape=(None,),
            dtype=types, chunks=(1000,),
            compression='gzip', compression_opts=7)

    def add(self, ds):
        oldmark = self.dataset.shape[0]
        self.dataset.resize(oldmark + ds.shape[0], axis=0)
        fields = self.dataset.dtype.names
        self.dataset[oldmark:] = np.asarray(ds[fields])
    def add_masked(self, ds, mask): 
        oldmark = self.dataset.shape[0]
        self.dataset.resize(oldmark + len(mask), axis=0)
        fields = self.dataset.dtype.names
        self.dataset[oldmark:] = np.asarray(ds[fields][mask])


def run():
    args = get_args()
    output_ds = args.out_dir.joinpath(args.dataset.name)
    output_ds.mkdir(parents=True, exist_ok=args.force)
    with File(output_ds.joinpath('all.h5'), 'w') as output:
        is_data = False if "data" not in str(args.dataset) else True
        build_slimmed_file_for(args.dataset, output, is_data=is_data)


if __name__ == '__main__':
    run()
