#!/usr/bin/env python3

"""
Calculates the signal efficiencies and background rejections using the discriminant 
histograms from make_roc_curves. It uses this information to plot ROC curves for 
discriminants, overlaid on same canvas. User can provide list of discriminants 
to overlay, but they must also have histograms made by make_roc_curves.

The ROC curves all have ratio panels with automatic y-range, but also produce
canvases with ratio panels with a zoomed in y-range, hard-coded here. To switch
off the ratio panels change DEFAULT_BASELINE_DISCRIM to None (not a string). 

This script is also able to produce a json file mapping WPs specified by user
to corrsponding background rejection, discriminant score for each discriminant 
user is studying. 

When we say process_hist, we refer to bin-values of a variable's histogram for the process

Input h5file structure: pt_range/discrim/dataset - dataset is [dijet_hist,higgs_hist,top_hist, edges]
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import json, os
from itertools import product
from pathlib import Path
from collections import defaultdict
import matplotlib.pyplot as plt

from xbb.common import dhelp 
from xbb.mpl import Canvas, CanvasWithRatio, log_style, add_atlas_label
from xbb.style import (
    DISCRIMINANT_NAME_MAP, DISCRIMINANT_NAME_MAP_SHORT,
    DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP,
    FlAV_NAME_MAP, FlAV_COLOR_MAP
)
from xbb.mpl import xlabdic, add_kinematic_acceptance
from matplotlib.ticker import MaxNLocator
#==================================================================
#Get arguments from CL
#==================================================================
#Help messages
#================
_DISCRIM_LIST_HELP = dhelp('Provide space separated list of Discriminants to draw ROC for')
_OUTDIR_HELP = dhelp('The directory where plots are saved')
_RATIO_HELP = dhelp('Discriminant to divide others by for ratio panel')
_WP_HELP = 'Specify a WP to write out the corresponding Rejection values for it'
_DEBUG_HELP = 'Use to make a plot of of efficiency vs discriminant bin to check WPs rejection values '
_ATLAS_APPROVED_HELP = 'Determines Plot displays ATLAS Internal (not approved) or ATLAS Preliminary (approved) '
_FLAVS_HELP = 'Flavour composition of large-R jet to overlay'
#================
#Defaults for Args 
#================
#DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V2_ftop025','mv2', 'dl1r','dl1r_VR'}
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V3_ftop025', 'dl1r'}
DEFAULT_BASELINE_DISCRIM = 'none'
DEFAULT_FLAVS = {'2b+0c', '1b+1c', '1b+0c', '2c+0b', '1c+0b', '0b+0c', 'all'}

#================
def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('h5_discrim_hists')
    parser.add_argument('-r', '--roc-discriminants', nargs='*', metavar='DISCRIM', default= DEFAULT_DISCRIMS, help =_DISCRIM_LIST_HELP)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/roc', help=_OUTDIR_HELP)
    parser.add_argument('-b', '--baseline-discrim', default=DEFAULT_BASELINE_DISCRIM, help =_RATIO_HELP)

    parser.add_argument('--flavs', default=DEFAULT_FLAVS, help=_FLAVS_HELP)
    parser.add_argument('-WP', '--working-point', nargs='+', type=int, metavar='WP', help=_WP_HELP,)#benchmark-values-pct
    parser.add_argument('--debug', action='store_true', help =_DEBUG_HELP)
    parser.add_argument('-a','--approved', action='store_true', help=_ATLAS_APPROVED_HELP)
    return parser.parse_args()
#==================================================================
# General Hard-coded settings 
#==================================================================
#Map needed if user wants specify discrims ratio panel y-range 
PT_RANGE_TO_RATIO_PANEL_YRANGE_MAP = {
    ('250_inf', 'dijet'): (0.5, 1.8),
    ('500_inf', 'dijet'): (0.75, 2.5),
    ('750_inf', 'dijet'): (0.5, 2.5),
    ('250_inf', 'top'): (0.5, 1.8),
    ('500_inf', 'top'): (0.75, 2.5),
    ('750_inf', 'top'): (0.5, 2.5),
}

#==================================================================
# main() 
#==================================================================
def run():

    #Get args
    args = get_args()
    #input files 
    h5_discrim_hists = args.h5_discrim_hists
    discrims_asked = args.roc_discriminants
    flavours = args.flavs
    discrim_to_divide_by = args.baseline_discrim
    if(discrim_to_divide_by=='none'):   discrim_to_divide_by=None
    WPs = args.working_point

    #================
    # Prepare Output directory
    #================
    #Get out-dir
    outdir = args.out_dir
    outdir.mkdir(parents=True, exist_ok=True)

    #================
    # Run methods
    #================
    pt_range_to_discrims_to_histos_map = prepare_input_data(h5_discrim_hists,discrims_asked )
    process_roc_curves(pt_range_to_discrims_to_histos_map,outdir,args,flavours,signals=['higgs'],bkg_processes = ['dijet','top'], make_zoomed_ratio_plot = True ) 

    #If user asked to write out bkg efficiencies (Rejections) for some WPs, prepare the info
    if(WPs):    get_WP_info(pt_range_to_discrims_to_histos_map,WPs,outdir,discrim_to_divide_by,signals=['higgs'])
#==================================================================
# Get the data from input file ready 
# this function depends on the structure of input file and nesting 
#==================================================================   
def prepare_input_data(input_file,discrims_asked):
    #================================
    # Open input file and get the discriminant histos from it
    #================================
    with File(input_file,'r') as h5file:
        pt_range_to_discrims_to_histos_map = defaultdict(dict)
        for pt_range_name, pt_range_group_contents in h5file.items():
            # Declare dict of form (key,(key,value)) = (discrim_name,(dataset,discrim_hist/edges))
            discrims_to_histos_map = {}
            # Get the pT range in form range = (min,max)
            pt_range_tuple = pt_range_group_contents.attrs['pt_range']
            # Get all available discriminant names from make_1d_roc file 
            available_discrim_names = [disc_name for disc_name in pt_range_group_contents.keys()]
            # Check all discriminants asked are available from the make_2d_roc step 
            if(any(discrim_asked not in available_discrim_names for discrim_asked in discrims_asked)):
                for discrim_asked in discrims_asked:
                    if(discrim_asked not in available_discrim_names):
                        print(f'Requesting a discriminant {discrim_asked} which has not been made in make_2d_roc.py ..')
                exit(0)    

            # Loop over discrims which we built histos for, checking user wants a ROC for it
            for discrim_name, discrim_group in pt_range_group_contents.items():
                flav_to_dataset_name_to_data_map ={}
                if discrim_name not in discrims_asked:   continue
                # Loop over flavours
                for flavour, flavour_group in discrim_group.items():
                    flav_to_dataset_name_to_data_map[flavour] = {proc_name: np.asarray(flavour_group[proc_name]) for proc_name in flavour_group.keys()}
                    flav_to_dataset_name_to_data_map[flavour]['edges'] = flavour_group.attrs['edges']

                # Map discrim to its associated datasets (key,(key,value)) = (discrim_name,(dataset, discrim_hist/edges))
                discrims_to_histos_map[discrim_name] = flav_to_dataset_name_to_data_map
            
            pt_range_to_discrims_to_histos_map[pt_range_name] = (pt_range_tuple,discrims_to_histos_map)
           

    return pt_range_to_discrims_to_histos_map

#==================================================================
# Function that calls all drawing methods with appropriate agrs  
#=================================================================    
def process_roc_curves(pt_range_to_discrims_to_histos_map,outdir,args,flavours,signals=['higgs'],bkg_processes = ['dijet','top'], make_zoomed_ratio_plot = True, **plot_args):
        #Get args
        discrim_to_divide_by = args.baseline_discrim
        if(discrim_to_divide_by=='none'):   discrim_to_divide_by=None

        is_plot_atlas_approved = args.approved
        debug = args.debug
        # Loop over pT ranges 
        for pt_range_name, (pt_range_tuple,discrims_to_histos_map) in pt_range_to_discrims_to_histos_map.items():
            # Destination directory for this pT range 
            out_path = outdir/pt_range_name
            out_path.mkdir(parents=True, exist_ok=True) 
                
            for bkg_process_name in bkg_processes: 
                get_roc_canvas_ready_and_draw(bkg_process_name,
                                                discrims_to_histos_map,
                                                flavours,
                                                out_path,
                                                pt_range_tuple,
                                                discrim_to_divide_by=discrim_to_divide_by, 
                                                debug=debug,
                                                approved = is_plot_atlas_approved,
                                                **plot_args)
                 
                #If user wants plots with ratio panel 
                if(make_zoomed_ratio_plot):
                    #Out directory where the plots with specfied ratio panel y-range will go 
                    special_ratio_panel_outdir = out_path/'zoomed_in_ratio_panel'
                
                    #Make the plots for dijet 
                    special_ratio_panel_range = PT_RANGE_TO_RATIO_PANEL_YRANGE_MAP.get((pt_range_name, bkg_process_name))
                    get_roc_canvas_ready_and_draw(bkg_process_name, 
                                                    discrims_to_histos_map,
                                                    flavours,
                                                    special_ratio_panel_outdir,
                                                    pt_range_tuple,
                                                    discrim_to_divide_by=discrim_to_divide_by,
                                                    zoomed_ratio_range=special_ratio_panel_range,
                                                    approved = is_plot_atlas_approved,
                                                    **plot_args)            

#==================================================================
# Function which takes in requested WPs in percentages and 
# produced the discriminant cut values corresponding to those WPs
# and the background rejections at these WPs (1/bkg eff)
# and the relative bkg rejection WRT the baseline discriminant
#=================================================================  
def get_WP_info(pt_range_to_discrims_to_histos_map,WPs,outdir,discrim_to_divide_by,signals=['higgs']):

        for pt_range_name, (pt_range_tuple,discrims_to_histos_map) in pt_range_to_discrims_to_histos_map.items():
            signal_eff_values_for_WPs = [x / 100 for x in WPs]
            #fill the WP to bkg eff map  (key,(key,value))=(pt_range,((disc_name,signal_name, bkg_name),eff_at_WP))
            disc_sig_bkg_to_bkg_rej_disc_score_at_wp_map = get_benchmark_bkg_rej_discrim_score(discrims_to_histos_map, 
                                                                            signal_eff_values_for_WPs)
            # Prepare output path for json file with WP info 
            out_path = outdir/pt_range_name
            json_dir = out_path/'WPs_info'
            json_dir.mkdir(parents=True, exist_ok=True)
            
            # Open the json file we will write to 
            with open(json_dir/'WP_bkgRej_discrimScore.json', 'w') as json_out:
                outdic = {}
                outdic['pt_range_' + pt_range_name] = json_vars(disc_sig_bkg_to_bkg_rej_disc_score_at_wp_map, signals=signals, baseline=discrim_to_divide_by)
                json.dump(outdic, json_out, indent=2)


#==================================================================
# Get ROC Curve Canvas Settings    
#==================================================================
def get_roc_canvas_ready_and_draw(bkg_process_name, discrims_to_histos_map, flavours,
                        out_dir,pt_range_tuple,  
                        sig_process_name='higgs',
                        discrim_to_divide_by = None,
                        zoomed_ratio_range=None, 
                        debug=False, 
                        approved=False,
                        rejection_max=None):
    #==================================================================
    # Prepare Some Canvas Settings 
    #==================================================================
    bkg_is_dijet = False
    bkg_is_top = False
    if(bkg_process_name == 'dijet'): bkg_is_dijet = True 
    if(bkg_process_name == 'top'):   bkg_is_top = True 
    if(bkg_is_dijet):
        ylabel = 'Multijet Rejection'
        backup_ylim = 2e8
        pdf_name = 'dijet.pdf'
    elif(bkg_is_top):
        ylabel = 'Top Rejection'
        backup_ylim = 1e9
        pdf_name = 'top.pdf'
    else:
        ylabel = ''
        backup_ylim = 0
        pdf_name = ''
        print("Background process not found so plot cannot be produced...")
        exit(0)
    if(sig_process_name == 'higgs'): xlabel = 'Higgs Efficiency'
    #==================================================================
    #Choose which Canvas Class to initialize (either with a ratio panel or not)
    Can = CanvasWithRatio if (discrim_to_divide_by is not None) else Canvas 
    for discrim_name, flav_to_procoess_to_hist_map in discrims_to_histos_map.items():
        
        # with Canvas(out_dir/'debug'/discrim_name/f'{pdf_name}') as canv:
        #     tot_sig = np.sum(discrims_to_histos_map[discrim_name]['0b+0c'][sig_process_name])
        #     tot_bkg = np.sum(discrims_to_histos_map[discrim_name]['0b+0c'][bkg_process_name])
        #     canv.ax.step(discrims_to_histos_map[discrim_name]['0b+0c']['edges'][:-1], discrims_to_histos_map[discrim_name]['0b+0c'][sig_process_name]/tot_sig, label='signal')
        #     canv.ax.step(discrims_to_histos_map[discrim_name]['0b+0c']['edges'][:-1], discrims_to_histos_map[discrim_name]['0b+0c'][bkg_process_name]/tot_bkg, label='bkg')
        #     canv.ax.legend(fontsize=26)
        #     log_style(canv.ax)
        
        with Can(out_dir/discrim_name/pdf_name) as can:
            for flav in flavours:
                #If a discriminant to divide by is provided, use it's histograms as baselines 
                if(discrim_to_divide_by is not None ): 
                    baseline_sig_histo = discrims_to_histos_map[discrim_to_divide_by][flav][sig_process_name]
                    baseline_bkg_histo = discrims_to_histos_map[discrim_to_divide_by][flav][bkg_process_name]
                    baseline_sig_sq = discrims_to_histos_map[discrim_to_divide_by][flav][sig_process_name+'_sq']
                    baseline_bkg_sq = discrims_to_histos_map[discrim_to_divide_by][flav][bkg_process_name+'_sq']                      
                else:   
                    baseline_sig_histo, baseline_bkg_histo, baseline_sig_sq, baseline_bkg_sq = np.array([]),np.array([]),np.array([]),np.array([])
                # Loop over the discriminants of interest, retrieving their histograms which can
                # are split by process 
                sig, bkg = flav_to_procoess_to_hist_map[flav][sig_process_name], flav_to_procoess_to_hist_map[flav][bkg_process_name]
                sig_sq, bkg_sq  = flav_to_procoess_to_hist_map[flav][sig_process_name+'_sq'], flav_to_procoess_to_hist_map[flav][bkg_process_name+'_sq']
                # Do the actual drawing on the Canvas 
                draw_roc(can, flav, sig, bkg, sig_sq, bkg_sq, out_dir, 
                         baseline_sig=baseline_sig_histo, 
                         baseline_bkg=baseline_bkg_histo,
                         baseline_sig_sq = baseline_sig_sq, 
                         baseline_bkg_sq = baseline_bkg_sq,                    
                         label=flav, 
                         debug=debug)  

            #Log-scale settings 
            log_style(can.ax)
            #ATLAS name on the plot + Internal or Prelimenary 
            add_atlas_label(can.ax, internal=(not approved))
            #Add the pT selection to the list of cuts printed on label 
            #By default, we will also get the Higgs mass range cut and "eta" cut printed 
            add_kinematic_acceptance(can.ax, x=0.25, y=0.73, pt_range=pt_range_tuple)
            can.ax.set_ylabel(ylabel, fontsize=32)
            
            #can.ax.set_ylim(None,rejection_max or backup_ylim)
            can.ax.legend(**legend_opts(approved),loc='upper right')
            
            # set the bottom of the plot to be 1, turn off ticks
            can.ax.set_ylim(bottom=1.0, top = backup_ylim)
            can.ax.tick_params(bottom=False, which='both')
            
            if isinstance(can, CanvasWithRatio):
                set_ratio_style(can.ax2, discrim_to_divide_by, yrange=zoomed_ratio_range)
                can.ax2.set_xlabel(xlabel, **xlabdic(32))
                can.ax2.tick_params(axis='x', labelsize=28)
                can.ax2.tick_params(axis='y', labelsize=24)
                can.ax.tick_params(axis='y', labelsize=28)

            else: 
                can.ax.set_xlabel(xlabel, **xlabdic(32))
                can.ax.tick_params(axis='both', labelsize=28)

    #==================================================================
# Some tiny plot styling methods 
#==================================================================
def set_ratio_style(ax, ratio, yrange=None):
    discrim = DISCRIMINANT_NAME_MAP_SHORT[ratio]
    ax.set_ylabel(f'Ratio to {discrim}', fontsize=20)
    if yrange is not None:
        ax.set_ylim(yrange)
    #ax.yaxis.set_major_locator(MaxNLocator('auto',prune='upper'))
def legend_opts(approved):
    opts = dict(frameon=False, fontsize=28)
    #if approved:
    #opts['bbox_to_anchor'] = (1.0, 0.88)
    return opts
#==================================================================
# Function where all the fun happens
# Calculate sig eff and corresponding bkg rejection then plot them on canvas 
# This is called once per discriminant, to overlay ROCs for discriminants on 1 Canvas 
# Here "this_discrim" refers to the discriminant being plotted on main canvas  
#==================================================================
def draw_roc(canvas, flav, sig, bkg, sig_sq, bkg_sq, out_dir, baseline_sig=None, baseline_bkg=None,
             baseline_sig_sq=None, baseline_bkg_sq=None,
             label='UNKNOWN', min_eff=0.4, debug=False):

    #======================================
    # Calculate signal eff and bkg rejection 
    #======================================
    sig_eff, bkg_rej,sig_eff_err, bkg_rej_err = get_sig_eff_bkg_rej_tuple(sig , bkg, sig_sq, bkg_sq, min_sigEff = min_eff, debug=debug,out_dir=out_dir)#,discrim_name=DISCRIMINANT_NAME_MAP[label])
    #======================================
    # Plotting the curve (finally...)
    #======================================
    #Get the line styling 
    this_label = FlAV_NAME_MAP[label]
    this_color = FlAV_COLOR_MAP[label]
    this_linestyle = '--'
    #plot on canvas - save the curve to get x,y data from it for ratio panel if needed
    canvas.ax.plot(sig_eff, bkg_rej, 
                    label=this_label,
                    color=this_color,
                    linestyle=this_linestyle,
                    linewidth=5)

    canvas.ax.fill_between(sig_eff, bkg_rej-bkg_rej_err,bkg_rej+bkg_rej_err,
                            color=this_color, alpha=0.1)
    # markerfacecolor = 'none'
    # markeredgecolor = this_color
    # markeredgewidth = 2 
    # canvas.ax.errorbar(sig_eff, bkg_rej, 
    #                 yerr= bkg_rej_err,
    #                 xerr = sig_eff_err,
    #                 marker='o', markersize=1,
    #                 markerfacecolor=markerfacecolor,
    #                 markeredgecolor=markeredgecolor,
    #                 markeredgewidth=markeredgewidth,
    #                 linestyle='none',
    #                 label=this_label,
    #                 color=this_color)

    #======================================
    # Deal with Ratio Panels if canvas is one with ratio panel 
    #====================================== 
    #Check canvas is from the class CanvasWithRatio
    if isinstance(canvas, CanvasWithRatio):
        #get the sig eff and bkg rej for the baseline discrim 
        baseline_sig_eff, baseline_bkg_rej = get_sig_eff_bkg_rej_tuple(baseline_sig,baseline_bkg,baseline_sig_sq,baseline_bkg_sq,min_sigEff=min_eff)
        
        this_discrim_sig_eff = sig_eff 
        this_discrim_bkg_rej = bkg_rej 
        
        # We have to do an interpolation to find the bkg rejection 
        # using the baseline discriminant at the same efficiency as
        # the plotted (numerator) discriminant 
        from scipy import interpolate
        #setup the function which does the scan 
        bkg_rej_finder = interpolate.interp1d(baseline_sig_eff, baseline_bkg_rej,bounds_error=False)
        interp_baseline_rej = bkg_rej_finder(this_discrim_sig_eff)

        # Set the ratio of rejections as y-axis on ratio panel 
        ratio_of_rejections = np.ones_like(interp_baseline_rej)
        ratio_of_rejections = this_discrim_bkg_rej/interp_baseline_rej
        #======================================
        # Plotting the curve 
        #======================================
        #plot on canvas
        canvas.ax2.plot(this_discrim_sig_eff,ratio_of_rejections,
                            label=this_discrim_label,
                            color=this_discrim_color,
                            linestyle=this_discrim_linestyle,
                            linewidth=3)


#==================================================================
# Function to return a tuple of sig eff and bkg rej
# if in debug mode, also plots some sanity check plots but user must provide some extra info
# to save the plot properly  
#==================================================================

def get_sig_eff_bkg_rej_tuple(signal,background,signal_sq, bkg_sq, min_sigEff = 0.4,debug=False,out_dir=None,discrim_name=None):
    #==============
    #Signal eff: 
    #=============
    #Get the cumilative sum of truth-matched starting from every bin edges (reverse cumsum)
    sig_eff_num = np.cumsum(signal[1:-1][::-1])[::-1] # exclude overflow and underflow
    #Get the total number of truth-matched jets -- include over/under flow to underestimate eff 
    sig_eff_denom = (np.cumsum(signal[::-1])[::-1]).max()
    #Signal efficiency is ratio 
    sig_eff = sig_eff_num/sig_eff_denom


    sig_eff_num_sq = np.cumsum(signal_sq[1:-1][::-1])[::-1] # exclude overflow and underflow
    sig_eff_denom_sq = (np.cumsum(signal_sq[::-1])[::-1]).max()
    sig_eff_sq = sig_eff_num_sq/sig_eff_denom_sq
    sig_eff_error = np.sqrt(abs( ( (1. - 2. * sig_eff) * sig_eff_num_sq  + (sig_eff**2) * (sig_eff_denom_sq) ) / sig_eff_denom**2))


    #Any eff below 0.4 is rubbish, don't use it 
    valid_eff = sig_eff > min_sigEff
    #=============
    #bkg rejection: 
    #=============
    bkg_num = np.cumsum(background[1:-1][::-1])[::-1] # exclude overflow and underflow
    bkg_denom = np.cumsum(background[::-1])[::-1].max() # include overflow and underflow
    bkg_eff = bkg_num/bkg_denom

    bkg_eff_num_sq = np.cumsum(bkg_sq[1:-1][::-1])[::-1] # exclude overflow and underflow
    bkg_eff_denom_sq = np.cumsum(bkg_sq[::-1])[::-1].max() # include overflow and underflow
    bkg_eff_sq = bkg_eff_num_sq/bkg_eff_denom_sq
    
    bkg_eff_error = np.sqrt(abs( ( (1. - 2. * bkg_eff) * bkg_eff_num_sq  + (bkg_eff**2) * (bkg_eff_denom_sq) ) / bkg_denom**2))
    

    
    #prepare array for rejection -- rejection is taken as 0 if eff = 0.0
    bkg_rej = np.zeros_like(bkg_eff)
    bkg_rej_sq = np.zeros_like(bkg_eff)
    bkg_rej_err = np.zeros_like(bkg_eff)
    valid = bkg_eff > 0.0
    #Only fill rejection elements if eff was not 0, else use rejection = 0
    bkg_rej[valid] = 1/bkg_eff[valid]
    bkg_rej_sq[valid] = 1/bkg_eff_sq[valid]
    
    bkg_rej_err = abs(bkg_eff_error*bkg_rej**2)

    
    

    return (sig_eff[valid_eff], bkg_rej[valid_eff],sig_eff_error[valid_eff], bkg_rej_err[valid_eff])
#==================================================================
# Function that takes in a discrim to histo mapping and desired signal eff value
# and returns each discrim's score corresponding to that sig eff based on the histos 
#==================================================================
def get_benchmark_bkg_rej_discrim_score(discrims_to_histos_map, working_points_eff,signal='higgs'):
    benchmark_bkgRej_discrimScore = {}

    signals = [signal] # do we need to loop over many signals in any scenario? 
    
    #Loop over the dictionary elements with discriminant names (keys)
    #and datasets (values) which are either histograms of processes or common edges of histograms 

    for disc_name, dataset_name_to_data_map in discrims_to_histos_map.items():
        #get the cut value to achieve a certain signal effiency 
        edges = dataset_name_to_data_map['edges']
        for sig in signals:
            sig_hist = dataset_name_to_data_map[sig]
            for dataset_name, data in dataset_name_to_data_map.items():
                #do some renaming for clarity 
                if(dataset_name=='edges'):  continue
                elif(dataset_name==sig):  continue
                else:   
                    bkg_hist = data
                    bkg_name = dataset_name

                sig_eff, bkg_rej = get_sig_eff_bkg_rej_tuple(sig_hist,bkg_hist, min_sigEff= -1)
                #need interpolation to find bkg eff and discrim score corresponding to sig eff we want 
                from scipy import interpolate
                #setup the function which does the scan for interpolation 
                #Find out bkg efficiency 
                bkg_rej_finder = interpolate.interp1d(sig_eff, bkg_rej,bounds_error=False)
                interp_bkg_rej = bkg_rej_finder(working_points_eff)
                #Find out score 
                bin_centers = (edges[:-1] + edges[1:]) / 2
                score_finder = interpolate.interp1d(sig_eff, bin_centers[1:-1],bounds_error=False)
                interp_score = score_finder(working_points_eff)

                print(working_points_eff)
                numpy_interp_scores = np.interp(working_points_eff, sig_hist[::-1].cumsum()/sig_hist.sum(), edges[:-1][::-1])
                print(numpy_interp_scores)
                # Save the score in a map 
                benchmark_bkgRej_discrimScore[disc_name, sig, bkg_name] = (interp_bkg_rej,interp_score,working_points_eff)

    return benchmark_bkgRej_discrimScore
#==================================================================
# Function that is responsible for making an appopriate dictionary to map
# a WP to its corresponding bkg rejection and discriminant score for each discrim
# studied. It also has entries for discrim_rej/baseline_rej for each WP 
#==================================================================
def json_vars(disc_sig_bkg_to_bkg_rej_disc_score_at_wp_map, signals=['higgs'], baseline=None):
    #Final output dict has form (key,val) = (sig, dict ) w
    outdic = {}

    for signal in signals:
        #Use a default discrim to take care of missing values?
        #complex, but final practical output of it is WP: (bkg_rej, disc_score) and WP:bkg_rej_ratio
        defdict = defaultdict(dict)
        # Loop over entries of the map produced by get_benchmark_bkgRej_discrimScore
        for (disc, sig, bkg), (bkg_rejs, disc_scores, WPs) in disc_sig_bkg_to_bkg_rej_disc_score_at_wp_map.items():
            if sig != signal or bkg == signal:  continue
            #The WPs are taken to be the signal efficiencies being studied 
            sig_effs = np.array(WPs)
            #prepare dict common keys 
            defdict[disc][bkg] = {}
            #Fill the dict with WP: (bkg_rej, disc_score)
            for sig_eff, bkg_rej, disc_score in zip(sig_effs*100, bkg_rejs, disc_scores):
                defdict[disc][bkg][f'{sig_eff:.0f}%_WP_Bkg_Rej'] = bkg_rej
            for sig_eff, disc_score in zip(sig_effs*100, disc_scores):
                defdict[disc][bkg][f'{sig_eff:.0f}%_WP_Score'] = disc_score
            
            #Here it is assumed baseline discrim is also in list of studied discrims 
            if baseline is not None:    
                #get the basline bkg_rej
                baseline_bkg_rej = disc_sig_bkg_to_bkg_rej_disc_score_at_wp_map[baseline, sig, bkg][0]
                #Fill the dict with WP: bkg_rej_ratio
                for sig_eff, bkg_rej_ratio in zip(sig_effs*100, bkg_rejs/baseline_bkg_rej):
                    defdict[disc][bkg][f'{sig_eff:.0f}%_WP_Rel_Bkg_Rej'] = bkg_rej_ratio
        #fill the final output dict 
        outdic[signal] = dict(defdict)
    return outdic



if __name__ == '__main__':
    run()
