#!/usr/bin/env python3

"""
This produces a 2D contour where:
    - x-axis is Signal Eff
    - y-axis is Variable (pT, eta,..)
    - z-axis is bkgEff_i/bkgEff_baseline where user specifies all (i)
        discrims and the baseline discrim. Otherwise, defaults are used
        that are consistent with default discrims from make_2d_roc
The contours are produced by tesselating the data using Delaunay Triangulation
then doing an Ndimensional linear interpolation

User can allow for some diagonistics plots to be produced under the out-dir specified

This uses the output of make_2d_roc.py
"""

from argparse import ArgumentParser
from glob import glob
import os
from pathlib import Path

import numpy as np
from h5py import File
from scipy.interpolate import LinearNDInterpolator
from scipy.spatial import Delaunay

from xbb.mpl import Canvas, helvetify, xlabdic, ylabdic
from xbb.mpl import add_atlas_label_in_box, add_kinematic_acceptance
from xbb.style import (DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP, DISCRIMINANT_NAME_MAP_SHORT)
from xbb.common import dhelp
from xbb.var_getters import VARIABLE_GETTERS, VARIABLE_EDGES

from matplotlib.colors import LogNorm, Normalize
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FuncFormatter, MaxNLocator

# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_VERBOSE_HELP = 'Turn on Verbose run'
_DENOM_HELP = 'The .json file containing DSID:NEventsProcessed Map '
_XS_HELP = 'The .txt file containing space separated info on XS and filter eff'
_OUTDIR_HELP = dhelp('Path to directory where ROC curves are saved ')
_ROC_DISC_HELP = dhelp('The ROC discriminants to plot curves for')
_DIAGNOSTICS_HELP = dhelp('Request Eff vs Var or Triangulation plots to be used as cross-checks')
_RATIO_HELP = dhelp('Discriminant to divide others by for ratio panel')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
# ================
# Defaults for Args
# =================
DEFAULT_PT_RANGE = (250e3, np.inf)
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V3_ftop025', 'dl1r'}
DEFAULT_BASELINE_DISCRIM = 'Xbb_V1_ftop025'
DISC_CHOICES = set(VARIABLE_GETTERS.keys())


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('roc_2d_file', type=Path)
    parser.add_argument('--var', required=True)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/roc2d', help=_OUTDIR_HELP)
    parser.add_argument('--verbose', action='store_true', help=_VERBOSE_HELP)
    parser.add_argument('-r', '--roc-discriminants', nargs='+', choices=DISC_CHOICES,
                        default=DEFAULT_DISCRIMS, help=_ROC_DISC_HELP)
    parser.add_argument('-b', '--baseline-discrim', choices=DISC_CHOICES,
                        default=DEFAULT_BASELINE_DISCRIM, help=_RATIO_HELP)
    parser.add_argument('-d', '--diagnostics', choices={'delaunay', 'efficiency'}, default=set())
    parser.add_argument('-a', '--approved', action='store_true')
    parser.add_argument('-p', '--pt-range', nargs=2, type=float,
                        default=DEFAULT_PT_RANGE, help=_PT_RANGE_HELP)

    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():

    # Get args
    args = get_args()

    hist2d_file = args.roc_2d_file  # Input files
    var_to_plot = args.var  # Variable to Plot
    discrims_asked = args.roc_discriminants  # Discriminants to Study
    baseline_discrim = args.baseline_discrim  # Discriminant to compare others to
    draw_diagnostics = args.diagnostics  # Make Diagonstics plots?
    is_plot_atlas_approved = args.approved  # Plot ATLAS approved?

    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    outdir = args.out_dir
    outdir.mkdir(parents=True, exist_ok=True)

    # ================
    # Run methods
    # ================
    (pt_range_to_discrims_to_proc_to_histos_map,
        pt_range_to_discrims_to_limits_map) = prepare_input_data(hist2d_file, discrims_asked,
                                                                 baseline_discrim)
    process_roc_curves(pt_range_to_discrims_to_proc_to_histos_map,
                       pt_range_to_discrims_to_limits_map,
                       var_to_plot, baseline_discrim,
                       outdir, signal='higgs',
                       bkg_processes=['dijet', 'top'],
                       approved=is_plot_atlas_approved,
                       diagnostics=draw_diagnostics)


# ==================================================================
# Get the data from input file ready
# this function depends on the structure of input file and nesting
# ==================================================================
def prepare_input_data(input_file, discrims_asked, baseline_discrim):

    # ================================
    # Open input file and get the 2D Histos of Var vs Discriminant
    # ================================
    with File(input_file, 'r') as histos_2d:
        # Mapping each pT range to its data
        pt_range_to_discrims_to_proc_to_histos_map = {}
        pt_range_to_discrims_to_limits_map = {}
        # Loop over the pT ranges produced by make_2d_rocs.
        for pt_range_name, pt_range_group in histos_2d.items():
            # Map from discriminant to process to histos for each pT range
            discrim_to_proc_to_histo_map = {}
            discrim_to_limits_map = {}
            # Get all available discriminant names from make_2d_roc file
            available_discrim_names = [disc_name for disc_name in pt_range_group.keys()]
            # Check all discriminants asked are available from the make_2d_roc step
            if(any(discrim_asked not in available_discrim_names for discrim_asked in discrims_asked)):
                for discrim_asked in discrims_asked:
                    if(discrim_asked not in available_discrim_names):
                        print(f'Requesting a discriminant {discrim_asked} \
                                which has not been made in make_2d_roc.py ..')
                exit(0)
            if(baseline_discrim not in available_discrim_names):
                print(f'Requesting a baseline discriminant {discrim_asked} \
                        which has not been made in make_2d_roc.py ..')
                exit(0)

            # Loop over discriminant in files, skipping ones that user didn't ask for
            for discrim_name, discrim_group in pt_range_group.items():
                if(discrim_name not in discrims_asked):
                    continue

                # Make a map discrim -> process -> prcoess_2D_histogram
                proc_name_to_histo_map = {proc_name: np.asarray(discrim_group[proc_name]['hist_wt'])
                                          for proc_name in discrim_group.keys()}

                discrim_to_proc_to_histo_map[discrim_name] = proc_name_to_histo_map
                # Array to hold the arr [(var_min, var_max), (discrim_min, discrim_max)]
                limits_arr = [discrim_group[proc_name].attrs['limits']
                              for proc_name in discrim_group.keys()]

                # Make sure that all limits are the same for the different processes
                assert all((x == limits_arr[0]).all() for x in limits_arr)

                # Use just one of the process limits since they're all the same for each discrim
                discrim_to_limits_map[discrim_name] = limits_arr[0]
            # Fill the map from pT range to discrim to process to process histos
            pt_range_to_discrims_to_proc_to_histos_map[pt_range_name] = discrim_to_proc_to_histo_map
            # Fill the map from pT range to discrim to discrim-var limits
            pt_range_to_discrims_to_limits_map[pt_range_name] = discrim_to_limits_map

    return pt_range_to_discrims_to_proc_to_histos_map, pt_range_to_discrims_to_limits_map


# ==================================================================
# Function responsible to process the data saved from input file
# and run an interpolation in 2D plane to draw a full 2D map
# and then call a drawing function to plot and save the rocs
# ==================================================================
def process_roc_curves(pt_range_to_discrims_to_proc_to_histos_map,
                       pt_range_to_discrims_to_limits_map,
                       var_to_plot, baseline_discrim, outdir,
                       signal='higgs', bkg_processes=['dijet', 'top'],
                       diagnostics=True, **plotargs):

    # Loop over pT ranges
    for pt_range_name, discrim_to_proc_to_histos_map in pt_range_to_discrims_to_proc_to_histos_map.items():
        # Directory for this pT range
        out_dir = outdir/pt_range_name
        out_dir.parent.mkdir(parents=True, exist_ok=True)
        # Map (discrim, bkg) -> bkg_eff
        discrim_bkg_to_bkgEff_map = {}
        # Map discrim -> limits
        interp_limits = {}
        # Loop over discriminants that the user asked for, from the file
        for discrim_name, process_to_hist_map in discrim_to_proc_to_histos_map.items():
            # Loop over the backgrounds
            for bkg in bkg_processes:
                # ================
                # Prepare data for interpolation
                # interp needed to fill discrim-var points
                # where no bkg-eff is saved
                # ================
                # The limits for the discriminant

                discrim_lims = pt_range_to_discrims_to_limits_map[pt_range_name][discrim_name]
                # Prepare a directory to save any roc diagnostics in
                interp_results_outdir = out_dir/f'{bkg}_diagnostics'
                # Get the map of interpolated bkg_eff values and the axis limits
                # returned limits are based on the range of values we interpolated over
                (discrim_bkg_to_bkgEff_map[(discrim_name, bkg)],
                    interp_lims) = get_roc_interp(discrim_name, process_to_hist_map,
                                                  discrim_lims, interp_results_outdir,
                                                  signal=signal, background=bkg,
                                                  diagnostics=diagnostics)
            interp_limits[discrim_name] = interp_lims

        # Loop over our interpolated grid for each backgorund, and draw the histogram
        for (discrim, bkg), bkg_eff in discrim_bkg_to_bkgEff_map.items():

            # get the name of the bkg process (used in color bar)
            proc_name = PROCESS_NAME_MAP[bkg]
            # get names of discriminants being compared (used in color bar )
            num_name = DISCRIMINANT_NAME_MAP[discrim].replace("$", "")
            denom_name = DISCRIMINANT_NAME_MAP[baseline_discrim].replace("$", "")

            cb_label = (
                f'{proc_name} ratio: '
                rf'$\epsilon_{{\rm {num_name}}}$ / '
                rf'$\epsilon_{{\rm {denom_name}}}$'
            )

            # Divide the bkg eff from the current discriminant by the baseline one
            bkg_eff_ratio = discrim_bkg_to_bkgEff_map[(discrim, bkg)] / discrim_bkg_to_bkgEff_map[(baseline_discrim, bkg)]
            # Prepare output file
            out_path = out_dir/f'{discrim}_over_{baseline_discrim}'/f'{bkg}.pdf'
            out_path.parent.mkdir(parents=True, exist_ok=True)
            # Draw the 2D histogram
            draw_2d_hist(bkg_eff_ratio, var_to_plot,
                         limits=interp_limits[discrim],
                         out_path=out_path,
                         discrim_name=discrim,
                         do_log=True,
                         cb_label=cb_label,
                         **plotargs)


# ==================================================================
# This function takes care of the interpolations needed to produce the 2D roc
# It makes a grid of signal eff vs variable, using Background Eff as z-axis
# Uses Delaunay triangulation and linear interpolations
# Also makes plots for diagnostics, based on the diagonstics mode in args
# ==================================================================
def get_roc_interp(discrim_name, discrim_data, discrim_var_limits, out_dir,
                   signal='higgs', background='dijet', diagnostics=set()):

    # ======================================
    # Get the Signal and Background Efficiencies
    # ======================================
    # get the histogram of the cumilative sum
    sig_eff_num = integrate(discrim_data[signal])
    bkg_eff_num = integrate(discrim_data[background])
    # =======
    # Signal
    # =======
    # get the total number of jets from the discriminant bins, for each variable bin
    sig_eff_denom = sig_eff_num[1:, :].max(axis=0)  # skip the cumsum over underflow bin
    # check denominator is not zero
    valid = (sig_eff_denom != 0)

    # get the efficiency -- denom size = variable binning size (e.g. 451+1(overflow))
    sig_eff_num[:, valid] /= sig_eff_denom[valid]
    # =======
    # Background
    # =======
    # get the background efficiency denominator
    bkg_eff_denom = bkg_eff_num[1:, :].max(axis=0)
    valid = (bkg_eff_denom != 0)
    bkg_eff_num[:, valid] /= bkg_eff_denom[valid]

    # ======================================
    # Prepare the variable being plotted against discrim
    # ======================================
    # Get variable Edges
    # there N-1 edges, -2 for overflow, +1 because of fenceposting (unpack with *)
    var_edges = np.linspace(*discrim_var_limits[1], sig_eff_num.shape[1] - 1)

    # ======================================
    # Make a Grid
    # We want X-axis to be Signal Efficiency, Y-axis is Variable, Z-Axis Bkg Rejection (color-coded)
    # ======================================
    # Get the axes, dropping underflow bins
    x, z = sig_eff_num[1:, 1:], bkg_eff_num[1:, 1:]
    # Get the values of the variable corresponding to each discriminant (using meshgrid [1])
    # Form : [[var0, var0,...],[var1, var1,...]]] where each nested array size = size(discrim edges)
    # Swap axes to get [[var0, var1,..],[var0, var1,..]] where each array size = size(var edges)
    y = np.meshgrid(x[:, 0], var_edges)[1].swapaxes(0, 1)
    # X in form [[disc1, disc2,..],[disc1, disc2,..],..] with size = size(var_bins)
    # Y in form [[var1, var2,..],[var1, var2,..],..] with size = size(disc bins)
    # Stack them such that [ [[disc1, var1],[disc2, var2] ,..],[[disc1, var1],[disc2, var2],..],..]
    x_vs_y = np.stack((x, y), 2)
    # flatten the stack, giving [[disc1, var1],[disc2, var2],.. ]
    # reshape(n,m) makes n arrays of size m from input (-1 is unknown dim)
    flat_xy = x_vs_y.reshape(-1, 2)
    # select the var edges, and get range = max-min value
    yrange = flat_xy[:, 1].ptp()
    # Divide the var edges by range
    flat_xy[:, 1] /= yrange
    # Use Delaunay Triangulation to tesselate the points
    grid = Delaunay(flat_xy)
    # Feed the triangulation results to linear Interpolator by Scipy which predicts the z values
    interp = LinearNDInterpolator(grid, z.flatten())
    # The values of x and y we would like to find z values for
    xint, yint = np.linspace(0.4, 1, 501), np.linspace(var_edges[0], var_edges[-1], 501)
    # stack the x and y values into form [ [[x1,y1],[x1,y2],..], [[x2,y1],[x2,y2],..],..]
    xy_interp = np.stack(np.meshgrid(xint, yint), 2).swapaxes(0, 1)
    xy_interp = np.array(xy_interp)
    # we don't care if we don't have a y-val for each x, because we are making the points
    xy_interp[:, :, 1] /= yrange
    # Get interpolated z values!
    z_interp = interp(xy_interp)

    # ======================================
    # Diagnostics
    # ======================================
    interp_grid_lims = [(lim.min(), lim.max()) for lim in [xint, yint]]

    out_dir.parent.mkdir(parents=True, exist_ok=True)

    # If user asks for efficiency plots, draw them from input file! (Eff vs Var HeatMaps)
    if 'efficiency' in diagnostics:
        # Plot Title
        title = rf'$1 / \epsilon_{{\rm bkg}}$ for {DISCRIMINANT_NAME_MAP[name]}'
        # path to these plots
        sig_eff_v_var_path = f'{out_dir}/effhist_{discrim_name}.pdf'
        z_interp_path = f'{out_dir}/zinterp_{discrim_name}.pdf'
        # Make plots !
        draw_2d_hist(sig_eff[1:, :], sig_eff_v_var_path, discrim_name, discrim_var_limits)

        draw_2d_hist(z_interp, z_interp_path, interp_grid_lims,
                     cb_label=title, discrim_name=discrim_name, do_log=False)

    # If user asked for a plot of the Delauny triangulation results
    if 'delaunay' in diagnostics:
        print(f'Drawing delaunay interp at {dpath}')
        # path to plots
        dpath = f'{out_dir}/delaunay_{discrim_name}.pdf'
        # Open a canvas and plot
        with Canvas(dpath) as can:
            # get the x-axis and y-axes (discrim vs var)
            flatx, flaty = flat_xy[:, 0], flat_xy[:, 1]
            # plot a scatter of the points
            can.ax.scatter(flatx, flaty, c=np.log(z.flatten()))
            # return of grid.simplices is arrays size 3, with indicies of pts making each triangle
            # plot thhe indicies
            can.ax.triplot(flatx, flaty, grid.simplices.copy())

    # If user asked for a plot of a Higgs Eff vs Bkg efficiency interpolated points
    if 'scatter' in diagnostics:
        spath = f'{out_dir}/interp_{discrim_name}.pdf'
        with Canvas(spath) as test:
            for var_val in range(0, len(var_edges), len(var_edges) // 5):
                xs = xy_interp[:, var_val, 0]
                zs = z_interp[:, var_val]
                v = ~np.isnan(zs)
                test.ax.scatter(xs[v], zs[v].max() / zs[v], label=f'{var_val}')
            test.ax.legend()
            test.ax.set_yscale('log')

    return z_interp, interp_grid_lims


# ==================================================================
# Integrating a 2D histogram
# If we have a lot of discrim bins we can drop some of them.
# We do this by setting the stride on the slice.
# The stride is the size of the step we take when we are slicing an arrat
# ==================================================================
def integrate(hist):
    # Get the stride: get size of discrim axis of histo, divde by 100 and compare quotient to 1
    # take quotient if it is bigger than 1 (if axis had 300 bins, use stride = 3)
    # This is calculated to be at least 1 and otherwise give between 100 and 200 bins.
    s = max(hist.shape[0] // 100, 1)
    # =============
    # Integrate along both axes separatly
    # =============
    # Reverse axis 0 (discrim) and get its cumsum array
    # Then reverse it back using the custom stride to skip bins
    hist = np.cumsum(hist[::-1, :], axis=0)[::-s, :]  # [:] makes a shallow copy of arr
    # Reverse axis 1 (var) and get its cumsum array, then reverse back
    hist = np.cumsum(hist[:, ::-1], axis=1)[:, ::-1]
    return hist


# ==================================================================
# Drawing 2D histograms
# ==================================================================
def draw_2d_hist(hist, var_to_plot, out_path, discrim_name, limits,
                 do_log=False, vrange=None, do_contours=False,
                 cb_label='Background Rejection Ratio vs DL1',
                 approved=False):

    # =============
    # The x and y ranges
    # =============
    if(var_to_plot == 'fatjet_pt'):
        # make the pt into TeV
        extent = (*limits[0], *(lim * 1e-6 for lim in limits[1]))
    elif(var_to_plot == 'fatjet_mass'):
        # make the mass into GeV
        extent = (*limits[0], *(lim * 1e-3 for lim in limits[1]))
    else:
        extent = (*limits[0], *(lim * 1 for lim in limits[1]))
    # =============
    # Plot settings
    # =============
    # A dictionary with the settings we need
    plot_args = dict(
        aspect='auto',
        origin='lower',
        extent=extent,
        interpolation='nearest',
        norm=LogNorm() if do_log else Normalize())

    if vrange is not None:
        plot_args['vmin'], plot_args['vmax'] = vrange

    # =============
    # Build Canvas and Plot
    # =============
    with Canvas(out_path, fontsize=24) as can:
        # =============
        # Get some text on the plot
        # =============
        # ATLAS label
        add_atlas_label_in_box(can.ax, 0.05, 0.95, internal=(not approved))
        # Kinematic selection text
        mass_in_presel = True
        if(var_to_plot == 'fatjet_mass'):
            mass_in_presel = False
        # Preselection Info
        add_kinematic_acceptance(can.ax, x=0.05, y=0.05, offset=0.05,
                                 mass_in_presel=mass_in_presel, in_a_box=True)
        #  takes an existing axes, creates a divider for it and returns an instance of AxesLocator.
        divider = make_axes_locatable(can.ax)
        # Create a new axis on a given side of original axis (color bar axis)
        cax = divider.append_axes("right", size="8%", pad=0.1)
        # Make the 2D plot
        im = can.ax.imshow(hist.T, **plot_args)
        # Set the x-axis label
        can.ax.set_xlabel('Higgs Efficiency', **xlabdic(28))
        # Set the y-axis label
        nice_var_name = DISCRIMINANT_NAME_MAP[var_to_plot]
        can.ax.set_ylabel(fr'Large-$R$ Jet {nice_var_name}', **ylabdic(28))
        # Set the x-axis ticks
        can.ax.xaxis.set_major_locator(MaxNLocator(steps=[1], prune='lower'))

        # =============
        # Colorbar
        # =============
        # Make a colorbar
        cb = can.fig.colorbar(im, cax=cax)
        # Format of numbers on the colorbar
        cb_fmt = FuncFormatter(format_cb_value)
        # Set the color bar label
        cb.set_label(cb_label, fontsize=28)
        # Formatting ticks
        cb.ax.yaxis.set_major_formatter(cb_fmt)
        cb.ax.yaxis.set_minor_formatter(cb_fmt)
        cb.ax.tick_params(labelsize=28, which='both')
        # =============
        # Contours (optional)
        # =============
        if do_contours:
            cont = can.ax.contour(
                hist.T, levels=[2, 5],
                linestyles='-', linewidths=10.0, alpha=0.5,
                origin='lower', extent=extent)
            cb.add_lines(cont)


def format_cb_value(value, pos):
    if abs(value - round(value)) < 0.001:
        return f'{value:.0f}'
    return f'{value:.1f}'


if __name__ == '__main__':
    run()
