#!/usr/bin/env python3

"""
Make histograms of discriminants
"""

from argparse import ArgumentParser, ArgumentTypeError
from h5py import File
from glob import glob
import numpy as np
import json
from sys import stderr
from pathlib import Path

from xbb.common import dhelp
from xbb.common import get_ds_weights_dict, get_dsid, get_pt_eta_reweighting
from xbb.selectors import PROC_SELECTORS
from xbb.selectors import (
    fatjet_mass_window_pt_range_truth_match,
    fatjet_mass_window_pt_range,
    mass_window_higgs, loose_mass_window
)
from xbb.var_getters import VARIABLE_GETTERS, VARIABLE_EDGES, DISCRIMINANT_GETTERS

# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_VERBOSE_HELP = 'Turn on Verbose run'
_NO_REWEIGHT_HELP = 'Turn off reweighting to QCD evaluation'
_WEIGHTS_HELP = 'The .json file containing DSID:Sample Weight Map '
_PT_ETA_HISTS_HELP = dhelp('Path to where the histograms fromjet_pt_eta_reweighting.py are stored')
_OUTFILE_HELP = dhelp('Path to (including) file where ROC curves are stored ')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
_CUTS_HELP = 'Expects VAR(sign)THRESHOLD. Sign can be =, geq or leq'
# ================
# Defaults for Args
# =================
DEFAULT_PT_RANGE = (250e3, np.inf)
DISC_CHOICES = set(VARIABLE_GETTERS.keys())
# ================


def threshold_type(arg):

    if (('=' not in arg) and ('geq' not in arg) and ('leq' not in arg)):
        raise ArgumentTypeError(f'cut thresholds {arg} should be specified as VAR=THRESHOLD')
    if('=' in arg):
        var, threshold_string = arg.split('=')
        operator = '=='
    elif('leq' in arg):
        var, threshold_string = arg.split('leq')
        operator = '<='
    elif('geq' in arg):
        var, threshold_string = arg.split('geq')
        operator = '>='
    else:
        var, threshold_string, operator = '', '', ''
    try:
        threshold = float(threshold_string)
    except:
        raise ArgumentTypeError(f"Couldn't parse {arg}")

    return var, operator, threshold


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('--var', default='dl1r')
    parser.add_argument('-w', '--weights', required=True, help=_WEIGHTS_HELP)
    parser.add_argument('-c', '--cuts', nargs='*', default=[],
                        type=threshold_type, help=_CUTS_HELP)
    parser.add_argument('-i', '--path-to-pt-eta-hists', default='pt-eta-hists',
                        type=Path, help=_PT_ETA_HISTS_HELP)
    parser.add_argument('-o', '--out-file', default='hist.h5', type=Path, help=_OUTFILE_HELP)
    parser.add_argument('-p', '--pt-range', nargs=2, type=float,
                        default=DEFAULT_PT_RANGE, help=_PT_RANGE_HELP)
    parser.add_argument('-v', '--verbose', action='store_true', help=_VERBOSE_HELP)
    parser.add_argument('--no-reweight', action='store_true', help=_VERBOSE_HELP)
    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():
    # Get args
    args = get_args()
    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    outfile = args.out_file
    outfile.parent.mkdir(parents=True, exist_ok=True)
    if(outfile.is_file()):
        outfile.unlink()

    # ================
    # Prepare fat-jet selectors
    # Must later pass the fat-jets datsets to them
    # ================
    pt_range = args.pt_range
    var = args.var 
    mass_window = mass_window_higgs
    if var == 'fatjet_mass': mass_window = loose_mass_window
    dijet_selector = fatjet_mass_window_pt_range(pt_range,mass_window)
    higgs_selector = fatjet_mass_window_pt_range_truth_match(pt_range,mass_window,{'GhostHBosonsCount':1})
    top_selector = fatjet_mass_window_pt_range_truth_match(pt_range,mass_window,{'GhostTQuarksFinalCount':1})
    #================
    # Prepare other selection criteria 
    #================      
    cuts = args.cuts+[('preselection','-',-np.inf)]
    #================
    # Process and save output per process
    # ================
    run_process('dijet', args, dijet_selector, cuts)
    run_process('higgs', args, higgs_selector, cuts)
    run_process('top', args, top_selector, cuts)


# ==================================================================
# Function to run a process, which means getting the histogram with appropriate settings
# and initiate save output
# ==================================================================
def run_process(process, args, preselection, cuts):
    # ================
    # Get all args
    # ================
    # skip reweighting?
    no_reweight = args.no_reweight
    # Be verbose?
    verbose = args.verbose
    # Get the destination directory
    outfile = args.out_file
    # The pt_eta_histos file to output to then use for reweights
    pt_eta_hists_file = args.path_to_pt_eta_hists/'jet_pt_eta.h5'
    # Overall sample weights file
    sample_weights_file = args.weights
    # Get the sample weights from file
    with open(sample_weights_file, 'r') as ds_wts_file:
        sample_weights = get_ds_weights_dict(ds_wts_file)
    # The sample datasets to process
    datasets = args.datasets
    # Get only datasets relevant to the current process
    valid_datasets = [ds for ds in datasets
                      if PROC_SELECTORS[process](get_dsid(ds))
                      and np.isfinite(sample_weights[get_dsid(ds)])]
    # pT range
    pt_range = args.pt_range
    # Variable to plot
    var = args.var
    # ================
    # Process the samples
    # ================
    if verbose:
        print(f'Processing {process} samples')
    # ================
    # Initialize Stuff
    # ================
    # Get pre-defined edges for discrim Histograms
    edges = VARIABLE_EDGES[var]
    # Choose new binning in case there is too many bins (that's not rebinning)
    n_bins = 100 if var in DISCRIMINANT_GETTERS.keys() else 50
    if edges.size > n_bins:
        edges = np.linspace(edges.min(), edges.max(), n_bins)
    # Get the variable we will plot
    var_getter = VARIABLE_GETTERS[var]

    proceses_per_cut = []
    # Prepare the selection criteria that will filter the data
    for cut_var, operator, cut_threshold in cuts:
        if verbose:
            print(f'Processing {cut_var} cut')
        # Apply the cut to the variable
        if cut_var in VARIABLE_GETTERS.keys():
            def cut(h5file):
                var_getter = VARIABLE_GETTERS[cut_var]
                if(operator == '>='):
                    return var_getter(h5file) > cut_threshold
                elif(operator == '<='):
                    return var_getter(h5file) < cut_threshold
                elif(operator == '='):
                    return var_getter(h5file) == cut_threshold
                else:
                    return var_getter(h5file) > cut_threshold

        elif(cut_var == 'preselection'):
            # Start with no cuts beyond pre-selection
            def cut(h5file):
                return np.ones(h5file['fat_jet'].shape, dtype=bool)
        else:
            print(f'Variable {cut_var} to cut on not in VARIABLE_GETTERS')
            continue

        # Initialize the histogram
        var_hist = 0

        def selection(h5file):
            return cut(h5file) & preselection(h5file['fat_jet'])

        # Loop over datasets for this process
        for ds in valid_datasets:
            if args.verbose:
                print(f'running on {ds} as {process}')

            # Get the histogram for this dataset (a numpy array with bin-values)
            this_dsid_var_hist = get_hist(ds, var_getter, edges,
                                          pt_eta_hists_file,
                                          selection, process,
                                          sample_weights,
                                          no_reweight=no_reweight)

            # Sum all histograms for non-dijet into a total discrim histogram
            var_hist += this_dsid_var_hist

        # Save the histogram to file
        save_hist(process, var_hist, var, edges, outfile, pt_range, cut_var, cut_threshold)


# ==================================================================
# Function which gets discriminant histogram bin-value for each sample dataset
# ==================================================================
def get_hist(ds, var_getter, edges, pt_eta_hists_file, selection, process,
             sample_weights, no_reweight=False):

    # ==================================================================
    # Getting re-weights for the histogram
    # ==================================================================
    (pt_reweighting, eta_reweighting,
        pt_edges, eta_edges) = get_pt_eta_reweighting(pt_eta_hists_file, process, get_edges=True)
    # Initialize the histograms
    var_hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath, 'r') as h5file:
            # Get fat-jets
            fat_jets = np.asarray(h5file['fat_jet'])
            # Get fat-jets pT array
            pt = fat_jets['pt']
            # Get the bin of each pT entry (length = pt array length)
            # if pt = (pt1,pt2) & pt1 in bin 1, pt2 in bin 5, then indicies = (1,5)
            indices_pt = np.digitize(pt, pt_edges) - 1
            # Overall event weight, for dijet = L*XS*filt_Eff / N_{MC}, 1 otherwise
            sample_weight = sample_weights[get_dsid(ds)]
            # Re-define weight as the qcd/proc ratio and re-size it to match pt array size
            # if ratio = [r1,r2,r3,r4,r5] and indices=[3,3,2,4,1,2,2,1,1,4,4] (size = pt.size())
            # weight = [r3,r3,r2,r4,r1,r2,r2,r1,r1,r4,r4] (size =pt.size())
            weight = pt_reweighting[indices_pt]*sample_weight*fat_jets['mcEventWeight']
            if(no_reweight):
                weight = sample_weight*fat_jets['mcEventWeight']
            # Get the array of discriminant values for each fat-jet
            var = var_getter(h5file)
            # Apply the selector function to the h5file (not just fatjets,for the sake of the cuts)
            sel = selection(h5file)
            # Make the numpy histogram and get the bin-values from it (element [0])
            # filtering disc array uses numpy bool indexing
            var_hist += np.histogram(var[sel], edges, weights=weight[sel])[0]
            if np.any(np.isnan(var_hist)):
                stderr.write(f'{fpath} has nans\n')

    return var_hist


# ==================================================================
# Function which writes out contents of var histo into H5 file
# It stores hist and edges in group pt_range/plot_var/cutvar_threshold/process
# ==================================================================
def save_hist(process, hist, plot_var, edges, outfile, pt_range, cut_var, cut_threshold):

    with File(outfile, 'a') as output:
        # Make a group named pT range to hold 1 group per cut to hold 1 group per var being plotted
        cut_threshold_str = 'none' if cut_threshold == -np.inf else f'{cut_threshold:.2f}'
        pt_str_gev = '_'.join('{:.0f}'.format(x*1e-3) for x in pt_range)
        outgroup_str = f'{pt_str_gev}/{plot_var}/cut_{cut_var}_{cut_threshold_str}/{process}'

        if outgroup_str in output:
            print(f'{outgroup_str} exists, not remaking')
            return
        outgroup = output.create_group(outgroup_str)
        # Hold the cut trheshold as group attribute
        outgroup.attrs['cut_threshold'] = cut_threshold

        # we need a bit of a hack here: float128 (longdouble) doesn't
        # get stored properly by h5py as of version 2.8
        outgroup.create_dataset('hist', data=hist, dtype=float, compression='gzip')
        outgroup.create_dataset('edges', data=edges, compression='gzip')


if __name__ == '__main__':
    run()
