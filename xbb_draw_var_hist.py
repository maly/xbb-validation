#!/usr/bin/env python3

"""
Draws the histograms made by make_hist into a file (e.g. pdf)

This is heavily reliant on the structure of the file produced by make_hist 
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict
from xbb.common import dhelp
from xbb.style import (
    DISCRIMINANT_NAME_MAP_SHORT,
    DISCRIMINANT_NAME_MAP,
    DISCRIMINANT_COLOR_MAP,
    PROCESS_COLOR_MAP,
    PROCESS_NAME_MAP,
)
#==================================================================
#Get arguments from CL
#==================================================================
#Help messages
#================
_OUTDIR_HELP = dhelp('Path to directory where ROC curves are saved ')
_EXT_HELP = dhelp('The extension of the plot files produced ')

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('hists_file', type=Path)
    parser.add_argument('-o', '--out-dir', default='plots/disc', type=Path, help=_OUTDIR_HELP)
    parser.add_argument('-e', '--extension', default='.pdf', help=_EXT_HELP)
    return parser.parse_args()
#==================================================================
# main() 
#==================================================================
def run():
    #Get args
    args = get_args()
    #Get out files extension 
    out_ext = args.extension
    #input files 
    hists_file = args.hists_file
    #================
    # Prepare Output directory
    #================
    #Get out-dir
    outdir = args.out_dir
    outdir.mkdir(parents=True, exist_ok=True)

    overlay_different_cuts_per_process(hists_file, outdir,out_ext)
    overlay_different_processes_per_cut(hists_file,outdir,out_ext)
#==================================================================
# Function that runs through the input histogram file and gets important 
# information from it -- it knows how to deal with the nested structure 
# of the file 
#==================================================================
def hist_file_iter(h5file, out_dir):

    for pt_range, pt_range_group in h5file.items():
        for plot_var, plot_var_group in pt_range_group.items():
            presel = plot_var_group['cut_preselection_none']
            for cut, cut_group in plot_var_group.items():
                yield pt_range, plot_var, cut, cut_group, presel 

#==================================================================
# Function that gets the histograms and draws them on canvas such that 
# different processes are overlaid in a file named after each cut applied 
#==================================================================
def overlay_different_processes_per_cut(hists_file,outdir,out_ext):
    
    with File(hists_file,'r') as h5file:
        for pt_range, plot_var, cut, cut_group, presel in hist_file_iter(h5file, outdir):
            cutvar_pieces = cut.split('_')[1:-1]
            cutvar = '_'.join(cutvar_pieces)
            process_cut_to_hist_map = {}
            for process_name, process_group in cut_group.items():
                cutval = process_group.attrs['cut_threshold']
                hist = np.asarray(process_group['hist'])
                edges = np.asarray(process_group['edges'])
                hist_color = PROCESS_COLOR_MAP[process_name]
                process_cut_to_hist_map[process_name,cutvar,cutval] = hist, hist_color
                
            outpath = outdir/pt_range/'overlay_processes'/plot_var/cut
            outfile = outpath / f'{plot_var}_hists_{cut}'
            outfile = outfile.with_suffix(out_ext)
                
            draw_hists(process_cut_to_hist_map, edges, plot_var,outfile,pt_range)
#==================================================================
# Function that gets the histograms and draws them on canvas such that 
# different cuts are overlaid in a file named after each process of interest
#==================================================================
def overlay_different_cuts_per_process(hists_file, outdir,out_ext):

    with File(hists_file,'r') as h5file:
        process_to_cut_to_hist_map = defaultdict(dict)
        for pt_range, plot_var, cut, cut_group, presel in hist_file_iter(h5file, outdir):
            cutvar_pieces = cut.split('_')[1:-1]
            cutvar = '_'.join(cutvar_pieces)
            cutvar_cutval_to_hist_map = {}
            
            for process_name, process_group in cut_group.items():
                cutval = process_group.attrs['cut_threshold']
                hist = np.asarray(process_group['hist'])
                edges = np.asarray(process_group['edges'])
                hist_color = DISCRIMINANT_COLOR_MAP[cutvar]
                process_to_cut_to_hist_map[process_name]['-',cutvar,cutval] = hist, hist_color
            
            for process_name, process_group in cut_group.items():      
                outpath = outdir/pt_range/'overlay_cuts'/plot_var
                outfile = outpath / f'{process_name}_{plot_var}_hist' 
                outfile = outfile.with_suffix(out_ext)
                y_label = PROCESS_NAME_MAP[process_name] + ' Jets'
                presel_hist = np.asarray(presel[process_name]['hist'])
                draw_hists(process_to_cut_to_hist_map[process_name],edges, plot_var,outfile,pt_range=pt_range,ylab=y_label, presel=presel_hist)


#==================================================================
# Function which draws the histograms on a canvas and saves them into a PDF 
# Takes care of legends, axis settings, etc 
#==================================================================
def draw_hists(label_descrip_to_hist_map, edges, var, outfile,pt_range=None, ylab='Jets',presel=None):
    from xbb.mpl import Canvas, CanvasWithRatio,  xlabdic, ylabdic, helvetify, add_atlas_label, add_kinematic_acceptance
    helvetify()

    centers = (edges[1:] + edges[:-1]) / 2
    with CanvasWithRatio(outfile) if presel is not None else Canvas(outfile) as can:
        # If we are plotting eff v mass, change mass cut text 
        mass_in_presel = True

        if(var == 'fatjet_mass'):    mass_in_presel = False      
        add_kinematic_acceptance(can.ax,pt_range=pt_range,mass_in_presel=mass_in_presel)
        add_atlas_label(can.ax, x=0.05, y=0.95, vshift=0.05)


        if(var == 'fatjet_pt'):   centers = centers*1e-6
        if(var == 'fatjet_mass'):   centers = centers*1e-3

    
        for (hist_name, cut_var, cut_val), (hist, color) in label_descrip_to_hist_map.items():
            proc_overlay = False
            cuts_overlay = False
            if hist_name in PROCESS_NAME_MAP.keys():
                proc_overlay = True
            else:
                cuts_overlay = True

            if(proc_overlay):
                nice_proc_name = PROCESS_NAME_MAP.get(hist_name)
                if(cut_val != -np.inf):
                    nice_cutvar_name = DISCRIMINANT_NAME_MAP_SHORT.get(cut_var,cut_var)
                    legend = nice_proc_name + f': {nice_cutvar_name}' + r'$ > ' + f'{cut_val:.2f}' + r'$'
                else:   legend = nice_proc_name
            if(cuts_overlay):   
                nice_cutvar_name = DISCRIMINANT_NAME_MAP_SHORT.get(cut_var,cut_var)
                if(cut_val != -np.inf):
                    legend = nice_cutvar_name + r'$ > ' + f'{cut_val:.2f}' + r'$'
                else:   legend =f'Preselection Jets'
            
            can.ax.step(centers, hist / hist.sum(), label=legend, color=color, linewidth=3)
            

            if(presel is not None):
                can.ax2.plot(centers, (hist/hist.sum())/(presel/presel.sum()), label=legend, color=color, linewidth=3)
        
        can.ax.set_ylabel(f'{ylab} (normalized)', **ylabdic())
        if(isinstance(can, CanvasWithRatio)):
            can.ax2.set_xlabel( DISCRIMINANT_NAME_MAP[var], **xlabdic())
        else:
            can.ax.set_xlabel( DISCRIMINANT_NAME_MAP[var], **xlabdic())
        
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0, maxval*1.3)
        can.ax.legend(fontsize=16, frameon=False)



if __name__ == '__main__':
    run()
