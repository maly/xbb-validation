Hbb Training Dataset Validation Scripts
=======================================

This repository contains several scripts to verify that the training
datsets for H->bb tagging are doing what we think they should be
doing. 

(REQUIRED) Getting the Data Ready 
=======================================
We will use as an example the mc16a p4258 samples from [xbb-datasets](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-datasets/)

After you download the H5 files from rucio into `<h5files_dir>`, you should 
get them ready to use using the following steps:

1. Create symlinks to the H5 files with shorter and user-friendly names:

```
./xbb_rename.py xbb-datasets/p4258/mc16a-input-datasets.txt xbb-datasets/p4258/mc16a-hbb-datasets.txt <h5files_dir> -o <h5_symlinks_dir>
```
which will use the full dataset names in `mc16a-input-datasets.txt` to make directories 
inside `<h5_symlinks_dir>` with meaniningful short names to hold H5 files per sample dataset

2. (optional) `xbb_make_truncated_file_list` :
```
./xbb_make_truncated_file_list.py <h5_symlinks_dir>/mc16a* -o <trunc_h5_symlinks_dir> -m 1000000
``` 

If running locally, this is very important if you don't want to wait an hour to 
get new plots everytime you make a change! It will cap the number of events processed
by the code to 1M events per process. It does this by making a new directory with 
symlinks to just enough H5 sample files until we have 1M event or next closest number 


(REQUIRED) Getting Sample Normalization Ready
=======================================
Since several stages are required to properly assign weights to
the samples, it's important that a few scripts be run first.

1. `xbb_get_overall_weights` :

```
./xbb_get_overall_weights.py <trunc_h5_symlinks_dir>/mc16a.* -x data/xsec.txt -o <weights_dir>
```

This calculates the overall normalization that should be applied to samples. 
It's given by $`w=\frac{L*\sigma*\epsilon}{N_{MC}}`$ for dijet samples, $`w = 1`$ 
otherwise. The weights will be in a `overall_sample_weights.json` file per DSID
used by other scripts.

Here `data/xsec.txt` is a file with every dijet sample DISID, XSection, Filter Efficiency.


2. `xbb_jet_pt_eta_reweighting` : 
```
 ./xbb_jet_pt_eta_reweighting.py <trunc_h5_symlinks_dir>/mc16a.* -w <weights_dir>/overall_sample_weights.json -o <pt_eta_hists_dir>
```
This processes the $`p_{T}`$ and $`\eta`$ distributions of all large-R jets
and saves them into an output file `jet_pt_eta.h5` used by other scripts. The
ratio of PROC/QCD pT/eta histograms is retrieved from this file using the method
`get_pt_eta_reweighting` in `xbb/common.py` and used to scale non-QCD spectra to 
QCD evaluation.

As a bonus, it will draw all the histogram it saves into the `.h5` files + some more.

3. 2D (pT v Eta) reweighting (under construction...)



Instructions for Making Plots
==========================================

The code can plot the following;
  - 1D ROC Curves for different discriminants overlaid 
    - w/out a ratio panel dividing by 1 discriminant 
    - (optional) Produces discriminant scores corresponding to 
      user-specified WPs and corresponding bkg rejections 
  - 2D ROC Curves of any variable against signal efficiency 
    - The z-axis in these plots is ratio of bkg efficiency 
      of any 2 discriminants
  - Histograms of any large-R jet variable 
    - Plots can be made with cuts on variables giving:
      - different processes overlaid per cut
      - different cuts overlaid per process 
  - Variable-cut efficiency versus variable (e.g. pT) plots 
    - Plots use a varying discriminant cut
      in each variable bin to achieve a flat signal efficiency
    - Plots overlaying the eff vs variable plots for different WPs
  - Fixed-cut efficiency versus variable (e.g. pT) plots 
    - Plots use a fixed cut on discriminant 
      independent of the variable bin.
    - Plots overlaying the eff vs variable plots for different WPs
    
  - 2D Variable Histogram (Under Construction ... )

Most plotting follows the same workflow:
  1) A `make` script, which prepares necessary histograms to extract 
     the data to be drawn from and saves them in an `.h5` file 
  2) A `draw` script, which calculates relevant info from `make` output 
     and plots it. 

1D ROC Curves 
================
- Make 1D ROC Curves: 

```
./xbb_make_1d_rocs.py <trunc_h5_symlinks_dir>/mc16a.* -w <weights_dir>/overall_sample_weights.json -o <roc1d_outfile> -r Xbb_V1_ftop025 Xbb_V3_ftop025 dl1r -i <pt_eta_hists_dir>
```
Notes:
  - `-r` : the names of discriminants to plot ROC for. Options in `xbb/var_getters.py` in the `DISCRIMINANT_GETTERS` map 
  - `-o` : (sort of optional) the path to the output H5 file, including the file name 
  - `-p` : (optional) the pT range to be applied to data (default is $`250->inf` \text{ GeV} $) in the form `-p min max` 
Output:
  - 1D Histograms (bin-values) and their edges of all Discriminants requested 

- Draw 1D ROC Curves

```
./xbb_draw_1d_rocs.py <roc1d_outfile> -r Xbb_V1_ftop025 Xbb_V3_ftop025 dl1r -b dl1r -o <roc1d_plots_outdir> -WP 50 60 70
```
Notes:
  - `-r` : the names of discriminants to plot ROC for. Their historgams must have been made by `./xbb_make_1d_rocs.py`
  - `-b` : (optional) the baseline discriminant to divde others by. It is used to make a background-rejection ratio panel
  - `WP` : (optional) produces discriminant cut-values and background rejections for the specified Working Points in a `.json` file
  - `--debug` : (optional) produces a few extra plots that may help with checking code behaviour 
 
2D ROC Curves 
================
- Make 2D ROC Curves :

```
./xbb_make_2d_rocs.py <trunc_h5_symlinks_dir>/mc16a.* --var <variable> -w <weights_dir>/overall_sample_weights.json  -o <roc2d_outfile> -r Xbb_V1_ftop025 Xbb_V3_ftop025 dl1r -i <pt_eta_hists_dir> 
```

Notes:
  - `--var`: can be `fatjet_pt`/ `fatjet_eta`/`fatjet_mass`
  - `--verbose` : (optional) prints a few extra statements as the code runs, useful for debugging 
  - `-p` : (optional) the pT range to be applied to data (default is $`250->inf` \text{ GeV} $) in the form `-p min max` 
  - Other arguments same as 1D ROC 

Output: 

  - 2D Histograms of discriminant vs variable, with Z-axis being number of selected jets 

- Draw 2D ROC Curves:

```
./xbb_draw_2d_rocs.py <roc2d_outfile> --var <variable> -r Xbb_V1_ftop025 Xbb_V3_ftop025 dl1r -b Xbb_V1_ftop025 -o <roc2d_plots_outdir> 
```

Notes:
  - The arguments are the same as in ROC 1D with addition of 
  - `--var`: can be `fatjet_pt`/ `fatjet_eta`/`fatjet_mass` and must match the one in `make` step 
  - The script will calculate efficiencies and perform interpolation to plot a 2D contour 
     of singal efficiency vs Variable, with a Z-axis of backgorund efficiency of each discriminant
     specfied by `-r` divided by discriminant specified by `-b`
  

Variable Histograms 
==========================================
Assuming steps 1-3 of the above instruction have been run:

- Produce histograms from chosen discriminant from DISCRIMINANT_GETTERS (e.g. --var dl1r)
```
./xbb_make_var_hist.py <trunc_h5_symlinks_dir>/mc16a.*  --var <variable>  -w <weights_dir>/overall_sample_weights.json -o <hist_outfile> -i <pt_eta_hists_dir>  -c Xbb_V1_ftop025geq2.49 Xbb_V3_ftop025geq2.46
```
Notes:
  - `--var`: can be `fatjet_pt`/ `fatjet_eta`/`fatjet_mass`
  - `-c` : list of cuts to apply to the histogram. The variables available to cut on are in `xbb/var_getters.py` under `VARIABLE_GETTERS` map
  - `--verbose` : (optional) prints a few extra statements as the code runs, useful for debugging 
  - `-p` : (optional) the pT range to be applied to data (default is $`250->inf` \text{ GeV} $) in the form `-p min max` 
  - `--no-reweight` : (optional) Turn off reweighting to QCD evaluation 

- Draw:
```
./xbb_draw_var_hist.py <hist_outfile> -o <hist_plot_outdir>
```

variable-cut Efficiency Plots 
==========================================

- Variable vs Efficiency Plots
```
./xbb_draw_constant_efficiency_plots.py <roc2d_outfile> --var <variable> -o <eff_plots_outdir> -r Xbb_V3_ftop025 Xbb_V1_ftop025 dl1r
```
where:
 - `--var`: can be `fatjet_pt`/ `fatjet_eta`/`fatjet_mass` and has to match variable in 2D rocs 

fixed-cut Efficiency Plots 
==========================================

- Variable vs Efficiency Plots
```
./xbb_inclusive_eff_plots.py <roc2d_outfile> --var <variable> -o <eff_plots_outdir> -r Xbb_V3_ftop025 Xbb_V1_ftop025 dl1r
```
where:
 - `--var`: can be `fatjet_pt`/ `fatjet_eta`/`fatjet_mass`

2D Variable Histograms (Under Construction...)
==========================================

(OUT-DATED) Example instructions for PUB note plots (To be Updated)
=======
* Figure 1
```
./xbb_make_jet_pt_hist.py -v -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d
python xbb_draw_hists.py plots_mc16d/jetpt.h5 --var pt -o plots_mc16d
```

* Figure 2
```
python xbb_make_disc_hist.py -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d --var dl1r -v

python xbb_make_disc_hist.py -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d --var Hbb_025 -v

python xbb_draw_hists.py plots_mc16d/Hbb_025.h5 -o plots_mc16d --var Hbb_025 -p dijet higgs top
python xbb_draw_hists.py plots_mc16d/dl1r.h5 -o plots_mc16d --var dl1r -p dijet higgs top

```
