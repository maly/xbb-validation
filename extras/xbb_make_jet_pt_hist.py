#!/usr/bin/env python3

"""
Make histograms of the jet pt and eta spectra and saving jet_pt_eta.h5 for later reweighting

Returns a file jet_pt_eta.h5 which contains:
    - 3 Groups: Higgs, Top and dijet 
      - Each group (a.k.a proc) contains:
        - A dataset with pT spectrum bin-values, called 'pt_hist'
        - A dataset with pT spectrum bin-edges, called 'pt_edges'
        - A dataset with eta spectrum bin-values, called 'eta_hist'
        - A dataset with eta spectrum bin-edges, called 'eta_edges'
    - A Group of datasets corresponding to samples summed up to make spectrum
    - A Group with ratio QCD['hist']/proc['hist'], called 'dijet_{process}_ratio'
      - A dataset with ratio bin-values, called 'pt_hist'
      - A dataset with ratio bin-edges = pT bin-edges, called 'pt_edges'
      - A dataset with ratio bin-values, called 'eta_edges'
      - A dataset with ratio bin-edges = pT bin-edges, called 'eta_edges'    
ds = dataset 
processes below often refer to different generation settings. E.G. different pT slices in dijets or
G* masses in Higgs 
"""
from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import json, os
from sys import stderr
from numpy.lib.recfunctions import append_fields

from xbb.common import get_ds_weights_dict, get_dsid, get_name, get_formatted_name, get_pt_eta_reweighting
from xbb.common import PROC_SELECTORS, dhelp 
from xbb.common import is_dijet, is_dihiggs, is_wz
from xbb.cross_section import CrossSections
from xbb.selectors import EVENT_LABELS, all_events, mass_window_higgs, truth_match_mass_window_higgs
#==================================================================
#Get arguments from CL
#==================================================================
_VERBOSE_HELP = 'Turn on Verbose run'
_DENOM_HELP = 'The .json file containing DSID:NEventsProcessed Map '
_XS_HELP = 'The .txt file containing space separated info on XS and filter eff'
_OUTDIR_HELP = dhelp('The destination directory where all files are saved')

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true', help = _VERBOSE_HELP)
    parser.add_argument('-w', '--weights', required=True, help = _DENOM_HELP)
    parser.add_argument('-o', '--out-dir', default='pt-hists', help = _OUTDIR_HELP)
    return parser.parse_args()
#==================================================================
# main() 
#==================================================================
def run():
    args = get_args()
    #make edges and add an underflow and overflow bins for any pT outside the edges
    edges_pt = np.concatenate([[-np.inf], np.linspace(250e3, 3e6, 51), [np.inf]]) 
    edges_eta = np.concatenate([[-np.inf], np.linspace(-2, 2, 51), [np.inf]]) 
    #The destination file where all histograms made will be stored 
    out_hists = os.path.join(args.out_dir, 'jet_pt_eta.h5')
    #If the file already exists, remove it (re-make)
    if os.path.isfile(out_hists):
        os.remove(out_hists)
    #run all the samples: Gets histogram, draws it (in PDF) and saves it (in H5)
    run_dijet(edges_pt,edges_eta, args)
    run_nonDijet_sample(edges_pt,edges_eta ,'higgs', args)
    run_nonDijet_sample(edges_pt,edges_eta ,'top', args)
    #Histograms from here are plotted but not saved 
    #These steps also save the QCD/PROC pT spectrum ratio as a function of pT 
    run_nonDijet_sample(edges_pt,edges_eta ,'higgs', args,reweight=True)
    run_nonDijet_sample(edges_pt,edges_eta ,'top', args,reweight=True)
#==================================================================
# Process dijet samples 
#==================================================================
def run_dijet(edges_pt,edges_eta, args):
    #Prepare overall normalization of dijet histograms L*XS*filt_Eff / N_{MC} 
    with open(args.weights, 'r') as ds_wts:
        sample_weights = get_ds_weights_dict(ds_wts)

    #Initialize the histogram and dictionary connecting process with histogram
    #process can be e.g. different dijet pT slices
    proc_hist_dict = {}
    pT_hist = 0
    eta_hist = 0
    #filter datasets by 
    # 1- DSID to get dijet ones 
    # 2- By checking their normalization is non-zero 
    valid_datasets= [ ds  for ds in args.datasets if is_dijet(get_dsid(ds)) and np.isfinite(sample_weights[get_dsid(ds)])]
    #Loop over filtered out datasets only 
    for ds in valid_datasets:
        dsid = get_dsid(ds)
        if args.verbose:    print(f'processing {ds} as dijet')
        #Get the histogram for this dataset (a numpy array with bin-values)
        this_dsid_hist_pt, this_dsid_hist_eta = get_hist(ds, edges_pt,edges_eta, selection =  mass_window_higgs, ds_wt= sample_weights) 
        #make a short representive dataset name (e.g. jz_{jzslice}) using regex
        name = get_name(ds)
        #fill dictionary with (key,value) = (name of dataset, histogram)
        proc_hist_dict[name] = np.array([this_dsid_hist_pt,this_dsid_hist_eta])
        #sum all histograms for dijet into a total pT/eta histogram 
        pT_hist += this_dsid_hist_pt
        eta_hist += this_dsid_hist_eta
    #Draw the pT/eta histogram (overlaid with individual constituent processes) in pdf
    draw_hist(pT_hist,eta_hist, edges_pt,edges_eta, args.out_dir, proc_hist_dict, file_name='dijet.pdf')
    #Save the pT/eta histogam in the jet_pt_eta.h5 file (and its constituent processes)
    save_hist(pT_hist,eta_hist, edges_pt,edges_eta, args.out_dir, 'jet_pt_eta.h5', 'dijet', proc_hist_dict)
#==================================================================
# Process any sample which is not Dijet (Higgs/Top)
#==================================================================
def run_nonDijet_sample(edges_pt,edges_eta, process, args, reweight=False):
    #Initialize histograms and process:histo dictionary 
    #Process here can be different BSM Higgs/Top mass settings 
    pT_hist = 0
    eta_hist =0 
    dijet_proc_ratio_hist = 0
    proc_hist_dict = {}
    with open(args.weights, 'r') as ds_wts:
        sample_weights = get_ds_weights_dict(ds_wts)
    #get the destination directory 
    out_dir = args.out_dir
    
    #define a selector function- this is just a function, doesn't do anything at this step 
    fatjet_mass_window_truth_natch_selector = truth_match_mass_window_higgs(EVENT_LABELS[process])
    
    #filter datasets by DSID to select Higgs/Top samples only 
    valid_datasets= [ ds  for ds in args.datasets if PROC_SELECTORS[process](get_dsid(ds))]
    #name of the output PDF 
    outplot_name = '%s.pdf'%process
    if(reweight):
        #change PDF name 
        outplot_name = '%s_reweight.pdf'%process
        ratio_pt,ratio_eta = get_pt_eta_reweighting(f'{out_dir}/jet_pt_eta.h5',process)
    #Loop over valid datasets 
    for ds in valid_datasets:
        #reset the variable holding this disid's histo before assigining it a new dataset's histo 
        this_dsid_hist=0
        if args.verbose:    print(f'processing {ds} as {process}')   
        #Get the histogram for this dataset (a numpy array with bin-values)
        if(reweight):
             this_dsid_hist_pt, this_dsid_hist_eta = get_hist(ds, edges_pt,edges_eta, 
                selection = fatjet_mass_window_truth_natch_selector,
                reweight= reweight,
                ratio_pt=ratio_pt,
                ratio_eta=ratio_eta,
                ds_wt= sample_weights
                )
        else:
            this_dsid_hist_pt, this_dsid_hist_eta = get_hist(ds, edges_pt,edges_eta, selection = fatjet_mass_window_truth_natch_selector,ds_wt=sample_weights)
        
        #make a short representive dataset name (e.g. jz_{jzslice}) using regex
        name = get_name(ds)
        #fill dictionary with (key,value) = (name of dataset, histogram)
        proc_hist_dict[name] = np.array([this_dsid_hist_pt, this_dsid_hist_eta])  

        #sum all histograms for process into a total pT/eta histogram     
        pT_hist += this_dsid_hist_pt
        eta_hist += this_dsid_hist_eta

    #Draw the pT/eta histogram (overlaid with individual constituent processes) in pdf
    #Here we draw the histogram regardless of whether it is re-weighted or not 
    draw_hist(pT_hist,eta_hist, edges_pt,edges_eta, out_dir, proc_hist_dict, file_name=outplot_name)
    
    if(reweight):
        #We don't save the reweighted histogram to the H5 file 
        #make a qcd/proc ratio histo, with edges = pT/eta edges -- it is like an pT/eta-bins vs ratio plot 
        dijet_proc_ratio_hist_pt,dijet_proc_ratio_hist_eta  = get_dijet_proc_ratio_hist(ratio_pt,ratio_eta, edges_pt,edges_eta)
        #save the ratio histogram in the jet_pt_eta.h5 file 
        save_hist(dijet_proc_ratio_hist_pt,dijet_proc_ratio_hist_eta, edges_pt,edges_eta, out_dir, 'jet_pt_eta.h5', 'dijet_'+process+'_ratio')
    else:
        #if we haven't been re-weighting, save the pT/eta histogram for the sample to jet_pt_eta.h5. 
        save_hist(pT_hist,eta_hist, edges_pt,edges_eta, out_dir, 'jet_pt_eta.h5', process, proc_hist_dict)


#==================================================================
#Function to return sum of numpy pT/eta histograms for one process by summing all its datasets
#it re-weights the pT/eta spectrum of Higgs and Tops by passing reweight=True, and 
#the ratio QCD/PROC at every pT/eta bin  
#==================================================================
def get_hist(ds, edges_pt,edges_eta, selection=all_events, ds_wt=1.0, reweight=False, ratio_pt=None,ratio_eta=None):
    #initialize the histograms 
    hist_pt= 0 
    hist_eta = 0
    #open all h5 files in dataset 
    for fpath in glob(f'{ds}/*.h5'):
        dsid = get_dsid(ds)
        with File(fpath,'r') as h5file:
            #Get fat-jet pT array 
            pt = h5file['fat_jet']['pt']
            eta = h5file['fat_jet']['eta']
            #Get the mc Event weight for each fat-jet * (XS weight (in dijet) or 1 (in non-dijet) )
            weight_pT = ds_wt[dsid]*h5file['fat_jet']['mcEventWeight']
            weight_eta =  ds_wt[dsid]*h5file['fat_jet']['mcEventWeight']
            #If we are reweighting, then we are dividing 2 histograms that have been weighted already
            if reweight:
                #get the bin of each pT/eta entry (length = pt array length)
                #if in pt = (pt1,pt2) & pt1 falls in bin 1, pt2 falls in bin 5, then indicies = (1,5)
                indices_pt = np.digitize(pt, edges_pt) - 1
                indices_eta = np.digitize(eta, edges_eta) - 1
                #re-define weight as the qcd/proc ratio and re-size it to match pT/eta array size 
                #if ratio = [r1,r2,r3,r4,r5] and indices=[3,3,2,4,1,2,2,1,1,4,4] (size = pT/eta.size())
                #weight = [r3,r3,r2,r4,r1,r2,r2,r1,r1,r4,r4] (size =pT/eta.size()) 
                weight_pT*=ratio_pt[indices_pt]
                weight_eta*=ratio_eta[indices_eta]
            #turn the weight into a numpy array and multiply by any extra weights in args
            mega_weights_pT = np.array(weight_pT, dtype=np.longdouble)
            mega_weights_eta = np.array(weight_eta, dtype=np.longdouble)
            #apply the selector function to the fat-jet 
            sel_index = selection(h5file['fat_jet'])
            #make the numpy histogram and get the bin-values from it (element [0])
            #filtering pT/eta array uses numpy bool indexing
            hist_pt += np.histogram(pt[sel_index], edges_pt, weights=mega_weights_pT[sel_index])[0]
            hist_eta += np.histogram(eta[sel_index], edges_eta, weights=mega_weights_eta[sel_index])[0]
            #check if any elements of the histograms are nan and report the files with issues
            if (np.any(np.isnan(hist_pt)) or np.any(np.isnan(hist_eta))):
                stderr.write(f'{fpath} has nans\n')
    return hist_pt,hist_eta
#==================================================================
#Function to return numpy histogram bin-values for qcd/proc ratios, binned using pT/eta binning 
#==================================================================
def get_dijet_proc_ratio_hist(ratio_pt,ratio_eta,edges_pt,edges_eta):
    pt_ratio_hist = 0
    eta_ratio_hist = 0
    #use trick so that data is just an array of size = edges, so every bin gets only "1 entry"
    #scale entry by the ratio, so that bin-values are the ratios 
    pt_ratio_hist += np.histogram(edges_pt[:-1], edges_pt, weights = ratio_pt)[0]
    eta_ratio_hist += np.histogram(edges_eta[:-1], edges_eta, weights = ratio_eta)[0]
    return pt_ratio_hist,eta_ratio_hist
#==================================================================
#Function to draw a histogram and save it in PDF -- called once per process 
#==================================================================
def draw_hist(pt_hist,eta_hist, edges_pt,edges_eta, out_dir, proc_hist_dict={}, file_name='dijet.pdf'):
    from xbb.mpl import Canvas, xlabdic, ylabdic, helvetify, add_atlas_label
    #helvetify()
    if not os.path.isdir(out_dir):  os.mkdir(out_dir)
    if not os.path.isdir(out_dir+'pT'):  os.mkdir(out_dir+'pT')
    if not os.path.isdir(out_dir+'eta'):  os.mkdir(out_dir+'eta')
    
    centers_pt = 1e-6 * (edges_pt[1:] + edges_pt[:-1]) / 2
    gev_per_bin = (centers_pt[2] - centers_pt[1]) * 1e3
    


    with Canvas(f'{out_dir}/pT/{file_name}') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, vshift=0.05, add_kinematic_info=True)
        #plot the total histogram 
        can.ax.plot(centers_pt, pt_hist)
        can.ax.set_yscale('log')
        can.ax.set_ylabel(f'jets * fb / {gev_per_bin:.0f} GeV', **ylabdic())
        can.ax.set_xlabel(r'Fat Jet $p_{\rm T}$ [TeV]', **xlabdic())
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0.1, maxval)
        #plot constituent processes histograms from dictionary  
        for proc_name, bin_vals in proc_hist_dict.items():
            pt_binvals = bin_vals[0]
            #legends can be ugly,terse or pretty (pretty is e.g. $M_G = X GeV$)
            can.ax.plot(centers_pt, pt_binvals, label=get_formatted_name(proc_name, 'terse'))
        can.ax.legend(ncol=2)
    
    centers_eta = (edges_eta[1:] + edges_eta[:-1]) / 2
    units_per_bin = (edges_eta[2] - edges_eta[1])        
    with Canvas(f'{out_dir}/eta/{file_name}') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, vshift=0.05, add_kinematic_info=True)
        #plot the total histogram 
        can.ax.plot(centers_eta, eta_hist)
        can.ax.set_yscale('log')
        can.ax.set_ylabel(f'jets * fb / {units_per_bin:.0f} Units', **ylabdic())
        can.ax.set_xlabel(r'Fat Jet $\eta$', **xlabdic())
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0.1, maxval)
        #plot constituent processes histograms from dictionary  
        for proc_name, bin_vals in proc_hist_dict.items():
            eta_binvals = bin_vals[1]
            #legends can be ugly,terse or pretty (pretty is e.g. $M_G = X GeV$)
            can.ax.plot(centers_eta, eta_binvals, label=get_formatted_name(proc_name, 'terse'))
        can.ax.legend(ncol=2)
#==================================================================
#Function to save a histogram to jet_pt_eta.h5 file with appropriate grouping by process 
#This is called once per process, and prcoess name is fed as group_name
#==================================================================
def save_hist(pt_hist,eta_hist, edges_pt,edges_eta, out_dir, file_name, group_name, proc_hist_dict={}):
    #if requested destination directory for jet_pt_eta.h5 file doesn't exist, create it 
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    with File(f'{out_dir}/{file_name}','a') as h5file:
        #create a group named after process
        hist_group = h5file.create_group(group_name)
        #Insiide the group 
        #create 2 datasets, one to hold total hist bin-vals of histo and other to hold bin-edges 
        # we need a bit of a hack here: float128 (longdouble) doesn't
        # get stored properly by h5py as of version 2.8
        hist_group.create_dataset('pt_hist', data=pt_hist, dtype=float)
        hist_group.create_dataset('pt_edges', data=edges_pt)
        hist_group.create_dataset('eta_hist', data=eta_hist, dtype=float)
        hist_group.create_dataset('eta_edges', data=edges_eta)
        #save a nested group which holds the bin-vals for the different constituent processes bin-vals
        for proc_name, bin_vals in proc_hist_dict.items():
            pt_binvals = bin_vals[0]
            eta_binvals = bin_vals[1]
            proc_grp = hist_group.require_group('processes')
            proc_grp.create_dataset(proc_name+'_pt', data=pt_binvals)
            proc_grp.create_dataset(proc_name+'_eta', data=eta_binvals)
if __name__ == '__main__':
    run()
