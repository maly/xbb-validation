#!/usr/bin/env python3

"""
Draw Pretty Histograms by class (Top, QCD, Higgs, ...)
"""
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')

from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
import os
from collections import defaultdict
from xbb.common import get_formatted_name
from xbb.style import(
    DISCRIMINANT_NAME_MAP,
    DISCRIMINANT_COLOR_MAP,
    PROCESS_COLOR_MAP,
    PROCESS_NAME_MAP)
from xbb.mpl import Canvas, CanvasWithRatio, log_style
from xbb.mpl import xlabdic, add_sim_info_in_box, add_kinematic_acceptance
from xbb.mpl import add_atlas_label

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('hists')
    parser.add_argument('-o', '--out-dir', default='plots', type=Path)
    parser.add_argument('--var', default='dl1r')
    return parser.parse_args()

def histiter(h5file, out_dir):
    for selection, vargroup in h5file.items():
        basepath = out_dir / 'by_process'
        for var, cutgroup in vargroup.items():
            for cut, procgroup in cutgroup.items():
                cutval = procgroup.attrs['threshold']
                path = basepath / selection / var / cut
                yield selection, var, cutval, procgroup, path

def plot_disc_hist(h5file,out_dir,var):

    edges = None
    proc_var_by_sel = defaultdict(dict)
    for sel, cutvar, cut, procgroup, out_path in histiter(h5file, out_dir):
        parts = {}
        for process, histgroup in procgroup.items():
            hist = np.asarray(histgroup['hist'])
            parts[process,cut] = hist, PROCESS_COLOR_MAP[process]
            proc_var_by_sel[process, cutvar][sel,cut] = (
                hist, DISCRIMINANT_COLOR_MAP[sel])
            this_edges = np.asarray(histgroup['edges'])
            if edges is None:
                edges = this_edges
            else:
                if not np.allclose(edges, this_edges):
                    raise Exception('edges incompatable')
        draw_disc_hist(parts, edges, var, out_path.with_suffix(".pdf"), ylab='Jets')
    
    for (process, variable), parts in proc_var_by_sel.items():
        path = out_dir / 'by_selection' / process / variable
        y_label = PROCESS_NAME_MAP[process] + ' Jets'
        draw_disc_hist(parts, edges, variable, path.with_suffix(".pdf"),ylab=y_label)

def draw_disc_hist(parts, edges, var, out_path, ylab='Jets'):
    from xbb.mpl import ylabdic, helvetify, sad_atlas_label
    helvetify()
    out_path.parent.mkdir(parents=True, exist_ok=True)
    centers = (edges[1:] + edges[:-1]) / 2
    with Canvas(out_path) as can:
        sad_atlas_label(can.ax, x=0.05, y=0.95, vshift=0.05)
        can.ax.set_ylabel(f'{ylab} (normalized)', **ylabdic())
        can.ax.set_xlabel( DISCRIMINANT_NAME_MAP[var], **xlabdic())
        for (name, cut), (hist, color) in parts.items():
            label = PROCESS_NAME_MAP.get(name)
            if not label:
                label = DISCRIMINANT_NAME_MAP.get(name,name)
            if cut != -np.inf:
                label = f'({label})' r' $ > ' f'{cut:.2f}' r'$'
            #can.ax.plot(centers, hist / hist.sum(), label=label, linewidth=3)
            can.ax.hist(edges[:-1],bins=edges,weights=1*hist / hist.sum(), label=label, linewidth=3, fill=False,color=color, histtype='step', stacked=False,density=False)
        name = DISCRIMINANT_NAME_MAP[var]
        can.ax.set_xlabel(name, **xlabdic())
        maxval = can.ax.get_ylim()[1]
        minval = can.ax.get_ylim()[0]
        can.ax.set_ylim(None,maxval*1.2)
        can.ax.legend(loc='upper right',frameon=False,fontsize=22)

def draw_hist(h5file,out_dir,var):
    from xbb.mpl import Canvas, xlabdic, ylabdic, helvetify
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    with Canvas(f'{out_dir}/{var}.pdf') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, vshift=0.05, add_kinematic_info=True)
        for process, group in h5file.items():
            hist, edges = group['hist'], group['edges']
            if var == "pt":
                centers = 1e-6 * (edges[1:] + edges[:-1]) / 2
                gev_per_bin = (centers[2] - centers[1]) * 1e3
            else: centers = (edges[1:] + edges[:-1]) / 2

            can.ax.plot(centers, np.array(hist), label=get_formatted_name(process,'process'),linewidth=3,histtype='step', stacked=False,density=False)
        can.ax.grid(True)
        can.ax.set_ylabel('Arbitrary Units', **ylabdic())
        
        name = var
        can.ax.set_xlabel(name, **xlabdic())
        maxval = can.ax.get_ylim()[1]
        minval = can.ax.get_ylim()[0]
        can.ax.set_ylim(None,maxval*1.2)
        if var == "pt": 
            can.ax.set_yscale('log')
            can.ax.set_ylabel(f'Jets / {gev_per_bin:.0f} GeV', **ylabdic())
            can.ax.set_xlabel(r'Fat Jet $p_{\rm T}$ [TeV]', **ylabdic())
            can.ax.set_xlim([0.25, 3])
            can.ax.set_ylim(None,maxval*70)
        can.ax.legend(loc='upper right',frameon=False,fontsize=22)

def run():
    args = get_args()
    discrims = {}
    out_dir = args.out_dir
    var = args.var
    with File(args.hists,'r') as h5file:
        if var in DISCRIMINANT_NAME_MAP:
            plot_disc_hist(h5file,out_dir,var)

        else:
            draw_hist(h5file,out_dir,var)


if __name__ == '__main__':
    run()
